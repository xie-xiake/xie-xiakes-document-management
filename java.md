# 泛型

## 为什么使用泛型

1 保证了类型的安全性

  比如一个集合如果不泛型那么各种不同的数据都可以存进去

```
ArrayList list=new ArrayList();
 list.add("aaa");
 list.add(111); // 编译通过
```

加入泛型之后就能直接约束类型

当重写为使用泛型时，代码不需要强制转换：

```
List<String> list = new ArrayList<String>();list.add("hello");String s = list.get(0); // no cast
```

2 避免了不必要的拆装箱

```
object a=1;//由于是object类型，会自动进行装箱操作。int b=(int)a;//强制转换，拆箱操作。这样一去一来，当次数多了以后会影响程序的运行效率。
```

使用泛型以后

```
public static T GetValue<T>(T a){　　return a;}public static void Main(){　　int b=GetValue<int>(1);//使用这个方法的时候已经指定了类型是int，所以不会有装箱和拆箱的操作。}
```

## 如何使用泛型

### 泛型类

泛型类可以理解为在类上的参数，加上属性

```
public class Generic<T> {
    
    private T key;

    public Generic(T key) {
        this.key = key;
    }

    public T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }
}
```

泛型类在创建对象的时候没有指定类型将成为object类型

泛型类多用于集合

子类也是泛型类，子类和父类的泛型类型要一致

class A<T> extends B<T>

子类不是泛型类，父类要明确泛型的数据类型

class A extends  B<String>

### 泛型接口

语法

Intorfacc   接口名称 <泛型标识>{

泛型标识 方法名();

} 

使用

1 实现类不是泛型类，接口要明确数据类型

