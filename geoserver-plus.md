# geoserver plus

# 消费者用户管理

##用户分平台用户和消费者用户，他们都有企业与部门

## 消费者用户表

| 字段       | 描述   |     |
| -------- | ---- | --- |
| id       |      |     |
| name     | 名称   |     |
| eid      | 企业id |     |
| username | 账号   |     |
| password | 密码   |     |
| phone    | 手机号  |     |
| salt     | 盐    |     |
| mail     |      |     |
|          |      |     |
|          |      |     |

## 消费者企业表

| 字段       | 描述  |     |
| -------- | --- | --- |
| id       |     |     |
| name     | 名称  |     |
| describe |     |     |
|          |     |     |

## 消费者部门表

| 字段   | 描述     |     |
| ---- | ------ | --- |
| id   |        |     |
| name | 部门名称   |     |
| eid  | 企业id   |     |
| pid  | 部门上级id |     |
|      |        |     |

## 消费者与部门关联表

| 字段   | 描述      |     |
| ---- | ------- | --- |
| id   |         |     |
| u_id | 消费者用户id |     |
| d_id | 部门id    |     |

# 管理者用户管理

## 管理者用户-企业-部门-用户与部门关联表

管理者表与消费者表 表名不一致  表结构完全一致，

### 管理者用户接口

#### 注册

接口 /adminUser/register

post

| 参数       | 描述   | 是否必填 |
| -------- | ---- | ---- |
| name     | 名称   | true |
| eid      | 企业id | true |
| username | 账号   | true |
| password | 密码   | true |
| phone    | 手机号  | true |
| mail     | 邮箱   | true |

#### 编辑

接口 /adminUser/edit

post



| 字段       | 描述   |                        |
| -------- | ---- | ---------------------- |
| id       |      | true                   |
| name     | 名称   | true                   |
| eid      | 企业id | true                   |
| password | 密码   | false 有代表修改密码没有代表没修改密码 |

#### 删除

接口 /adminUser/delate

DELETE

param1：id
