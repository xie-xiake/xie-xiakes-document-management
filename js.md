# 学习js网站

```
https://developer.mozilla.org/zh-CN/docs/Learn/JavaScript
```

# 原型

```
构造函数通过原型分配的函数是所有对象多共享的
js规定，每一个构造函数都有一个prototype属性，指向另一个对象
这个对象可以挂载函数，对象实例化不会多次创建原型上函数，节约内存
构造函数和原型对象的this 都指向实例化的对象 

 function Obj() {

 }
  Obj.prototype.sing=function () {
     console.log("跳舞")
  }
  let a=new Obj()
    a.sing()
```

## 原型链

对象的 ___proto__ __指向原型的prototype，原型的___proto__ __，指向object原型的___proto__ __，object原型的___proto__ __指向null

### 原型链查找规则

1 当访问一个对象的属性(包括方法)时,首先查找这个对象自身有没有该属性

2 如果没有就查找他的原型 （也就是__proto___指向的prototype原型对象__）

3 如果没有就查找Object 的___proto__ __ 一直找到null

4 可以使用instanceof 运算符用于加测构造函数的protptype属性是否出现在某个对象的原型链上

```
function Person() {
   this.eye=2
  this.head=1
}

 let a=new Person()
 let b=new Person()
  console.log(a === b) //false
  console.log(a.prototype===b.prototype)  //true
  console.log(a instanceof Person) //true
  console.log(a instanceof Object) //true
  console.log(a instanceof Array) //false
```

## 原型继承

```
function Person() {
   this.eye=2
  this.head=1
}

function Wohom() {

}
function Man() {

}

let wohom=new Wohom()
//原型对象指向 new Person()而不是Person  是因为Person只有一个 会导致牵一发动全身
wohom.prototype=new Person()
wohom.prototype.baby=function(){
  console.log('生孩子')
}
wohom.prototype.constructor=Wohom
console.log(wohom)

let man=new Man()
man.prototype=new Person()
man.prototype.constructor=Man
console.log(man)


```

## constructor

```
每个原型对象里面都有一个constructor属性
该属性指向该原型对象的构造函数

function Start() {

}
  console.log(Start.prototype.constructor)

  function Start() {   

}
 Start.prototype={
   //重新指回这个对象的构造函数
    contructor: Start
    sing:function () {
  console.log()
    }
 }
```



## 对象原型

```
new Start()
实例对象里面有一个属性叫做 _proto_,这个属性指向原型对象prototype
```

## 原型扩展

### 扩展数组

```
const  arr=[1,2,3]
    Array.prototype.max=function () {
    this指的是当前对象
      return  Math.max(...this)
    }

    console.log(arr.max())
```

# 解构赋值

## 数组解构

#### 说明:

数组解构是将数组的单元值快速批量赋值给一系列变量的简介语法

```
const arr=[1,2,3]
 const [mix,min,avg]=arr
 console.log(min)
 console.log(mix)
 console.log(avg)
 利用解构赋值交换变量
  let a=1
    let b=2;
  [b,a]=[a,b]

  let [a,b,...c]=[1,2,3,4,5,6]
  c为3 4 5 6
```

## 对象解构

```
const {name,age}={
    name:'扎根三',
    age:15
}
console.log(name)
   console.log(age)

   改名
    const {name:usname,age}={
     name:'扎根三',
     age:15
 }
 console.log(usname)
    console.log(age)

    解构数组对象
    const objArr=[{name:'张三',age:16}]
    const [{name,age}]=objArr
    console.log(name)
    console.log(age)

    解构嵌套对象
    const objArr={
    name:'张三',
    age:16,
    dog:{
       cc:'小狗',

    }

}

  const {name,age,dog:{cc}}=objArr

    console.log(name)
    console.log(age)
    console.log(cc)

    解构对象嵌套数组
    const  msg={
    "code":200,
    "msg":"请求成功",
    "data":[{
        "title":"标题"
    }]
}

const {data}=msg
console.log(data)

解构超级简单写法
const  msg={
    "code":200,
    "msg":"请求成功",
    "data":[{
        "title":"标题"
    }]
}
   function f({data}) {
        console.log(data)
   }
 f(msg)
```

# 闭包

## 概念

闭包 = 内层函数 + 外层函数的变量

外部可以访问函数内部的变量

闭包的坏处，造成内部变量一直被引用导致内存泄漏

```
   function f() {
      const a=1
       function f1() {
        console.log(a)
       }
       f1()
   }
   f()
```

# 垃圾回收机制

全局变量不回收，局部变量不用了会被回收

栈: 局部变量，基本数据类型放到栈里面

堆: 对象放堆里面

# 作用域

## 局部作用域

### 函数作用域

在函数内部，外部无法直接访问

1.函数内部声明的变量，在函数外部无法被访问

2.函数的参数也是函数内部的局部变量

3不同函数内部声明的变量无法互相访问

4.函数执行完毕后，函数内部的变量实际被清空了

### 块作用域

由{}包裹的作用域叫做块作用域

# 常量

使用const声明的量是常量，常量永远不会被改变

const  g=9.8

# 字符串类型

## 声明

以下三种都是字符串类型

let  a='aa'

let a="aa"

let a=`aa`

## 模板字符串

${age}  可以完成字符串的占位符，但只能在反引号里面``

## 字符串常用方法



# 类型

## undefined

 let a

声明了没有赋值就是undefined

## null

let a=null

赋值了但是为空

代表空

## Boolean类型

Boolean(内容)

''   0  undefined null false Nan 转换为布尔值后都是false，其余为true

## 类型转换

### 隐式转换

 1+'1'='11'  //加号左右两边如果有一个字符串，就会把另外一边也转成字符串

1-'1'=0 // 减号两边有一个字符串就把这个字符串转换为数字

+'111' // 加号字符串的方式可以把字符串转换为数字

### 显示转换

Number('111') //转换成数字类型

parseInt('12px')//转换成整数类型

parseFloat('abc12.65px') //转换成浮点数

# 运算符

## 比较运算符

 ===  强烈推荐使用 左右两边是否类型和值都相等

## 展开运算符

let arr=[1,2,3]

...arr=1,2,3

### 展开对象

```
const obj={
  usanme:'root'
}
const obj2={
  password:'root'
}

const obj3={...obj,...obj2}  


   function obj({host,port,...user}) {
      console.log(host)
      console.log(port)
      console.log(user.username)
      console.log(user.password)
   }
   obj({host: '127.0.0.1',port:3306,username:'张三',password:'root'})
```

# 数组

## 数组查找

### find

```
   const arr=[1,2,3,5]
    let a=  arr.find(item=>{
      return  item===2
    })
    console.lo
   g(a)如果没有就是undinfh
```

### every

```
  const arr=[1,2,3,5]
  let a=arr.every(item=>{item >=10})
    console.log(a) 
  true 或者false  
  数组中是否都满足指定条件 
    const arr=[1,2,3,5]
  let a=arr.some(item=>{item >=10})
    console.log(a) 
  true 或者false  
  数组中是否有元素满足指定条件 

```

## 数组求和

```
  const arr=[1,2,3,5]
  //数组求和 没有初始值
    const  total=arr.reduce(function (prev,current) {
      //prev 前一个
      // current 后一个
     return prev+current
    })
    //有初始值
    const  total2=arr.reduce(function (prev,current) {
    return prev+current
    },10)

    console.log(total)
    console.log(total2) 
prev 默认是0 如果没有初始值的话 随着遍历会成为后一个值 current 代表元素
```

## 合并数组

### 使用剩余参数合并

let  arr1=[1,2,3]

let arr2=[4,5]

const arr3=[...arr1,...arr2]



## 声明数组

  let arr=new Array(1,2,3)

let arr=[1,2,3]

## 数组新增

```
let arr=['111','2222']
let i=arr.push('666') //返回新数组的长度
  arr.unshift('kkkk') //往开头增加元素
```

## 数组删除

```
let arr=['111','2222']
arr.pop() //删除数组最后一个元素
arr.shift() //删除第一个元素
 arr.splice(0,1) //0代表选择的起始位置 1代表删除的个数
```

## 遍历数组

常规方式不做介绍

### map

  map可以遍历数组处理数据,并返回新的数组，他的重点在于有返回值for循环没有返回值

```
 const  arr=['red','blue','green']
    const newArr=arr.map(function (ele,index) {
      console.log(ele) //数组元素
      console.log(index) // 数组下标
      return  ele+'颜色'
    })
    console.log(newArr)
```

### join

join方法用于把数组中所有元素转换为一个元素，拼接成字符串

```
  const  arr=['red','blue','green']
    console.log(arr.join()) //小括号代表使用逗号拼接
    console.log(arr.join('')) // 空字符串代表没有分隔符
    console.log(arr.join('-')) // 空字符串代表-拼接
```

### forEach

```
const  arr=[1,2,3]

  arr.forEach((item,index)=>{
          console.log(item)
      console.log(index)
  })
```

# 函数

## 箭头函数

### 目的

语法间接

场景: 替换那些原本需要使用匿名函数的地方

### 语法

```

   let  fun=function () {
        console.log(123)
    }
    fun()

    let fun2=() =>{
        console.log(123)
    }
     fun2()
     括号内写形参

     let  fun3=x=>{
     console.log(x)
     只有一个形参的时候可以省略小括号
     }
     let  fun4=x=> consloe.log(x)
     只有一行代码的时候可以省略大括号
     let  fun5=x=>x+x
     只有一行代码可以省略return
     let  fun6=(uname)=>({name:uname})
     直接返回一个对象
```

## 函数参数

### 动态参数

```
 当用户需要传参 但不知道要传几个
 function f() {
   console.log(arguments)
  }
f(1,2)
arguments 是伪数组可以接收参数 这里存储的是 1 2
```

### 剩余参数

```
 语法是 ...参数名 是个数组 可以接收任何几个参数，而且他可以接收剩余的参数，这是一个真的数组
 function f(...arr) {
 ...arr ===1,2
   console.log(arr)
  }
f(1,2)
```

## 匿名函数

 let fun=funtion(){

}

fun() //匿名函数调用

匿名函数调用必须先声明再使用

## 逻辑中断

x= x || 0 //x为0 

y=y || 0 // y为0

当x和y是null或则undefined的时候，可以用后面的真值，这真是有趣的语言

# 对象

## 拷贝对象

### 浅拷贝

const  o={name:'张三',age:16}
const obj={}
Object.assign(obj,o)

还有一种写法

const obj={name:'张三'}

const o={...obj}

浅拷贝拷贝引用数据类型是复制地址值

### 深拷贝

```
const obj={
  name:'zhangsan',
  bady:{
    age:11
  }

}
 // 先转换为字符串
  let json=JSOn.stringify(obj)
// 字符串转化为对象
   const o=JSON.parse(json)
    //o 就是新的对象
```



## 创建对象

let  obj={}

let obj=new Object()

### 构造函数

语法规则: 函数名首字母必须大写,必须使用new来调用函数

这里的函数不能用箭头表达式

```
function Big(name,age) {
    this.name=name
    this.age=age
}

const obj=new Big("小猪",55)
    console.log(obj.name)
    console.log(obj.age)
```

### 实例成员

### 静态成员

构造函数的属性和方法称之为静态成员

```
function Big(name,age) {
    this.name=name
    this.age=age
}

Big.a=1

    Big.fun=function () {

    return 1;
    }


  console.log(Big.a)
  console.log(Big.fun())
```

### 常用静态方法

```
const  obj={name:'张三',age:16};
获得所有的属性名 搞成一个数组
const arr=Object.keys(obj)
获得所有的属性值 搞成一个数组
const arrValue=Object.values(obj)
console.log(arr)
console.log(arrValue)

//拷贝对象 把 o 拷贝给 obj
const  o={name:'张三',age:16}
const obj={}
Object.assign(obj,o)
```

### 删除对象属性

delate  obj.age

### 查询对象属性

```
let obj={
    'name':"bb"
}
console.log(obj['name'])
如果对象属性是带双引号 就用这种方式来访问
```

## 遍历对象

```
let obj={
    'name':"bb",
    age:1,
    gender:'男'
}
for(let k in obj){
    console.log(obj[k])
}
//k是属性名
因为k是字符串 所以要obj[k]来访问属性
```

# 事件监听

## 语法

 const bin=元素对象.addEventListener('事件类型',要执行的函数)

事件类型一定要加引号

## 事件类型

   用什么方式触发,比如鼠标点击click,鼠标经过mouseover等

# 日期

## 实例化

 const data=new Date();

获取当前时间

const date=new Date('2022-5-1 08:30:00')

指定时间

## 时间戳的使用

+new Date()

# M端事件

## 语法

   元素.addEventListener('touchstart',函数)

元素.addEventListener('touchend',函数)

元素.addEventListener('touchmove',函数)

# 异常处理

```
function f(x,y) {
  if(!x || !y){
      throw new Error('用户没有传递参数')
  }
  return x+y;
}
function f(x,y) {
   try {
       x+y
   } catch (e) {
       console.log(e.message)
   } finally {
       //不管程序对不对一定会执行
       console.log('弹出对话框')
   }
}
```

# 插件

## 网址

 [Swiper中文网-轮播图幻灯片js插件,H5页面前端开发](https://www.swiper.com.cn/)

# 类

```
// 通过class关键字创建类
class  Statr{
//类里所有的函数都不需要加funtioon
   constructor(unane) {
     this.unane=unane
   }


    say(){
       console.log('唱歌')
   }
}

let s=new Statr('刘德华')
```

## get&set

```
  class  Phone{

    get price(){

      return 'get被调用了'
    }
    set price(newValue){

      console.log('价格被修改了')
    }

  }
```

## 继承

```
class  Father{
   constructor(unane) {
     this.unane=unane
   }

   say(){
       console.log('唱歌')
   }
}

  class  Son  extends  Father{

        constructor(uname){
        //调用父类构造方法
        super(uname)
        }
        say(){
        //调用父类方法
        super.say()
        }
  }

  let son=new Son();
```

# Symbol

## 特点

他是为了解决代码扩展的危险问题

 Symbol的值是唯一的，用来解决命名冲突的问题

Symbol 值不能与其他数据进行运算

Symbol定义的对象属性不能使用 fon...in 循环遍历

但是可以使用Reflect.ownkeys 来获取对象的所有键名

## 创建

```
let s=Symbol()
let s2=Symbol('斤斤计较')
 let  s3=Symbol.for('踩单车对')
```

### 对象添加Symbol类型

```
let a={
    name:'张三'
}

 let name=Symbol()

  a[name]='李四'

  console.log(a)
```

# 生成器

## 作用

异步编程

## 语法

```
  function * f() {
      console.log("执行生成器函数方法")
  }
  console.log(f().next())
```

## yelid

```
  function * f(arg) {
      yield 111;
      yield  222;
      yield  333;
  }
  f().next()
  f().next()
  f().next() 
  参数的传递会成为yield的返回结果
```

```
// 生成器函数实例
    function one() {
        setTimeout(() => {
            console.log(111);
            genn.next();
        }, 1000)
    }

    function two() {
        setTimeout(() => {
            console.log(222);
            genn.next();
        }, 2000)
    }

    function three() {
        setTimeout(() => {
            console.log(333);
            genn.next();
        }, 3000)
    }
    function* gen() {

        yield one();

        yield two();

        yield three();
    }

    let genn = gen();
    genn.next();
```

## 传递参数

```
function* gen(arg) {
        console.log(arg);
        let one = yield 111;
        console.log(one);
        yield 222;
        yield 333;
    }

    // 执行获取迭代器对象
    let iterator = gen("qqq");
    console.log(iterator.next());
    console.log(iterator.next("bbb"));
    console.log(iterator.next());
    console.log(iterator.next());
```

# Promise

## 作用

ES6引入异步编程的新解决方案，用来封装异步操作并可以获取其成功或者失败的结果

## 基本语法

```
  const p=new Promise((resolve, reject)=>{
     //此处封装异步请求

     let data='数据库中的用户数据'
     // 第一个是成功状态的函数
     // resolve(data)

     let error='请求失败哦'
     //回调失败的函数
     reject(error)
   })
   //这是回调函数，封装了两个回调函数，第一个是成功状态的函数，第二个是失败状态的函数
   p.then((value)=>{
     console.log(value)
   },(reason)=>{
     console.error(reason)
   })
```

## then

.then方法返回的对象必须是Promise对象

# Set

set集合没什么好说的 只要是集合就可以用 for  of来遍历

for(let v  of  s2){

}

## 用set做数组去重

```
let  arr=[1,2,3,2,5,6,4,1,2,1]
    let setArr=[...new Set(arr)]
    console.log(setArr)
```

# Map

```
let m=new Map();
m.set('name','gg')
    m.set('fun',function () {
     console.log('值是函数')

  })  

   for(let v of m){
      console.log(v)
      //v是数组 第一个元素是键  第二个是值
    }
```

# 模块坏

# 好处

防止命名冲突 代码复用 高维护新

## 基本命令

### export:  用来规定模块化的暴露数据

#### 分别暴露

// 分别暴露  
export  let age=15  

export  let fun=function () {  
   console.log('模块化学习')  
}

### 统一暴露

// 统一暴露  
  let age=15  

  let fun=function () {  
   console.log('模块化学习')  
}  

export {age,fun}

### 默认暴露

```
export default {
    school: '学校',
  fun: function () {
   console.log('默认暴露')
  }

}

```

## import: 命令用来输入其他模块化提供的功能

### 通用导入 什么暴露都可以使用

```
 通用导入任何情况都可以使用
   <script type="module">
   import * as m1  from "./m3.js"

    console.log(m1)
  </script>
```

<script type="module">  
 import * as m1  from "./m1.js"  

  console.log(m1.fun())  
</script>

### 解构赋值导入 针对统一暴露

通过解构赋值来导入统一暴露

```
  <script type="module">
   import {age as a,fun as b}   from "./m2.js"


  </script>
```

### 默认导入 针对默认暴露

```
 默认导入对应的是默认暴露
   <script type="module">
   import {default as m3}  from "./m3.js"


  </script>
```

### 简便形式 针对默认暴露

```
import m3  from "./m3.js"
```

# async await

异步编程新的解决方案

## async

```
   async function f() {
     //只要结果不是Promise对象，那返回的就是一个成功的Promise对象
     return ;
   }
  const  result=f()    
    console.log(result)  

    async function f() {
     //只要结果不是Promise对象，那返回的就是一个成功的Promise对象
     return new Promise((resolve,reject)=>{
       resolve('成功')
     });
   }
  const  result=f()
   result.then(value => {
     console.log(value)
   },reason => {})  
    console.log(result)   

   1 async函数的返回值是promise对象
   2 promise对象的结果由async函数执行的返回值决定 

```

## await

```
    const  p=new Promise((resolve,reject)=>{
      // resolve('用户数据')

      reject('失败了')
    })
   async function f() {
      // await 后面跟Promise对象
     //如Promise对象返回是失败的需要用tru cath捕获
     try {
       let result= await p
       console.log(result)

     }catch (e) {
       console.log(e)
     }

   }

   console.log(f())
```

## 组合使用

```
function sendAjax(url) {
            return new Promise((resolve, reject) => {
                const xhr = new XMLHttpRequest()
                xhr.open('GET', url)
                xhr.send()
                // 处理返回结果
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        // 判断状态码
                        if (xhr.readyState >= 200 && xhr.readyState < 300) {
                            resolve(xhr.response)
                        }else {
                            reject(xhr.status)
                        }
                    }
                }
            })
        }  






```
