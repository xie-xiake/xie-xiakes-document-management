# C语言

## 预处理命令

前面各章中，已经多次使用过`#include`命令。使用库函数之前，应该用`#include`引入对应的头文件。这种以`#`号开头的命令称为预处理命令。

```
#include <stdio.h>
#if _WIN32
#include <windows.h>
#elif _linux_
#include <unistd.h>
#endif





int main(){

#if _WIN32
    Sleep(5000);
#elif Linux_
    sleep(5);
#endif

    printf("www.baidu.com");



    return 0;
}





```

```
对于 Windows 平台，预处理以后的代码变成：
#include <stdio.h>
#include <windows.h>
int main() {
    Sleep(5000);
    puts("http://c.biancheng.net/");
    return 0;
}  


对于 Linux 平台，预处理以后的代码变成：


#include <stdio.h>
#include <unistd.h>
int main() {
    sleep(5);
    puts("http://c.biancheng.net/");
    return 0;
}

```

## 宏文件定义

#define 叫做宏定义命令，它也是C语言预处理命令的一种。所谓宏定义，就是用一个标识符来表示一个字符串，如果在后面的代码中出现了该标识符，那么就全部替换成指定的字符串。

```
#include <stdio.h>

#include  "my.h"

#define  N 65


int main(){

    printf("%d\n", jbzd(1, N));

    return 0;
}





```

### 宏文件带参

```
#include <stdio.h>

#include  "my.h"

#define  N 65

#define  MAX(a,b) a>b?a:b


int main(){

    printf("%d\n", MAX(5,55));

    return 0;
}

参数记得带括号
#include <stdio.h>

#include  "my.h"

#define  N 65

#define  MAX(a,b) a>b?a:b

#define SQ(y) (y)*(y)


int main(){
    int num;
    printf("输入数字");
    scanf_s("%d", &num);

    printf("num=%d", SQ(num + 1));

    return 0;
}



```

### 宏参数的字符串化

#define STR(s) #s

printf("%s", STR(c.biancheng.net));

printf("%s", STR("c.biancheng.net"));

分别被展开为：

printf("%s", "c.biancheng.net");

printf("%s", "\"c.biancheng.net\"");

```
#include <stdio.h>

#include  "my.h"

#define  N 65

#define  MAX(a,b) a>b?a:b

#define SQ(y) (y)*(y)


#define  CON1(a,b)  a##b##55



int main(){
    int num;
    printf("输入数字");
    scanf_s("%d", &num);

    printf("num=%d  \n", SQ(num + 1));

    printf("%d", CON1(5, 66));

    return 0;
}





```

### 预定义宏

顾名思义，预定义宏就是已经预先定义好的宏，我们可以直接使用，无需再重新定义。  

ANSI C 规定了以下几个预定义宏，它们在各个编译器下都可以使用：

* __LINE__：表示当前源代码的行号；
* __FILE__：表示当前源文件的名称；
* __DATE__：表示当前的编译日期；
* __TIME__：表示当前的编译时间；
* __STDC__：当要求程序严格遵循ANSI C标准时该标识被赋值为1；
* __cplusplus：当编写C++程序时该标识符被定义。
  
  

```
#include <stdio.h>
#include <stdlib.h>
int main() {
    printf("Date : %s\n", __DATE__);
    printf("Time : %s\n", __TIME__);
    printf("File : %s\n", __FILE__);
    printf("Line : %d\n", __LINE__);
    system("pause");
    return 0;
}
```

### 条件编译

```
#if 整型常量表达式1
    程序段1
#elif 整型常量表达式2
    程序段2
#elif 整型常量表达式3
    程序段3
#else
    程序段4
#endif
```

#### #ifdef 的用法

#ifdef  宏名  
    程序段1  
#else  
    程序段2  
#endif

它的意思是，如果当前的宏已被定义过，则对“程序段1”进行编译，否则对“程序段2”进行编译。

### #ifndef 的用法

与 #ifdef 相比，仅仅是将 #ifdef 改为了 #ifndef。它的意思是，如果当前的宏未被定义，则对“程序段1”进行编译，否则对“程序段2”进行编译，这与 #ifdef 的功能正好相反。

### 三者之间的区别

最后需要注意的是，#if 后面跟的是“整型常量表达式”，而 #ifdef 和 #ifndef 后面跟的只能是一个宏名，不能是其他的。

## #error

#error 指令用于在编译期间产生错误信息，并阻止程序的编译，其形式如下：

例如，我们的程序针对 Linux 编写，不保证兼容 Windows，那么可以这样做：

1. #ifdef WIN32
2. #error This programme cannot compile at Windows Platform
3. #endif
   
   

## 预处理命令总结

| 指令       | 说明                             |
| -------- | ------------------------------ |
| #        | 空指令，无任何效果                      |
| #include | 包含一个源代码文件                      |
| #define  | 定义宏                            |
| #undef   | 取消已定义的宏                        |
| #if      | 如果给定条件为真，则编译下面代码               |
| #ifdef   | 如果宏已经定义，则编译下面代码                |
| #ifndef  | 如果宏没有定义，则编译下面代码                |
| #elif    | 如果前面的#if给定条件不为真，当前条件为真，则编译下面代码 |
| #endif   | 结束一个#if……#else条件编译块            |

## 头文件

.h就是头文件，头文件只能声明函数和全局变量，但不能定义他们

# 内存

内存字节计算公式

1GB

1G=1024MB，1MB=1024KB，1KB=1024字节

1GB=1 *  1024  * 1024 * 1024  字节

int main() {
    int a;
    printf("a的内存地址 %d\n", &a);
    a = 1;
    printf("a的内存地址 %d\n", &a);

}

先声明int 类型的 a变量 ，声明之后会分配给他一个地址值

由于int 类型需要四个字节 内存中会划分四个格子给他 并将第一个格子的内存地址给他

a=1   把1存在四个格子内  

# 程序解构

```
#include <stdio.h>

int main()
{
   /* 我的第一个 C 程序 */
   printf("Hello, World! \n");

   return 0;
}
```

接下来我们讲解一下上面这段程序：

1. 程序的第一行 _#include <stdio.h>_ 是预处理器指令，告诉 C 编译器在实际编译之前要包含 stdio.h 文件。
2. 下一行 _int main()_ 是主函数，程序从这里开始执行。
3. 下一行 /*...*/ 将会被编译器忽略，这里放置程序的注释内容。它们被称为程序的注释。
4. 下一行 _printf(...)_ 是 C 中另一个可用的函数，会在屏幕上显示消息 "Hello, World!"。
5. 下一行 **return 0;** 终止 main() 函数，并返回值 0。

# 屏幕输出

* | 格式控制符                          | 说明                                                                                                         |
  | ------------------------------ | ---------------------------------------------------------------------------------------------------------- |
  | %c                             | 输出一个单一的字符                                                                                                  |
  | %hd、%d、%ld                     | 以十进制、有符号的形式输出 short、int、long 类型的整数                                                                         |
  | %hu、%u、%lu                     | 以十进制、无符号的形式输出 short、int、long 类型的整数                                                                         |
  | %ho、%o、%lo                     | 以八进制、不带前缀、无符号的形式输出 short、int、long 类型的整数                                                                    |
  | %#ho、%#o、%#lo                  | 以八进制、带前缀、无符号的形式输出 short、int、long 类型的整数                                                                     |
  | %hx、%x、%lx<br>%hX、%X、%lX       | 以十六进制、不带前缀、无符号的形式输出 short、int、long 类型的整数。如果 x 小写，那么输出的十六进制数字也小写；如果 X 大写，那么输出的十六进制数字也大写。                    |
  | %#hx、%#x、%#lx<br>%#hX、%#X、%#lX | 以十六进制、带前缀、无符号的形式输出 short、int、long 类型的整数。如果 x 小写，那么输出的十六进制数字和前缀都小写；如果 X 大写，那么输出的十六进制数字和前缀都大写。               |
  | %f、%lf                         | 以十进制的形式输出 float、double 类型的小数                                                                               |
  | %e、%le<br>%E、%lE               | 以指数的形式输出 float、double 类型的小数。如果 e 小写，那么输出结果中的 e 也小写；如果 E 大写，那么输出结果中的 E 也大写。                                 |
  | %g、%lg<br>%G、%lG               | 以十进制和指数中较短的形式输出 float、double 类型的小数，并且小数部分的最后不会添加多余的 0。如果 g 小写，那么当以指数形式输出时 e 也小写；如果 G 大写，那么当以指数形式输出时 E 也大写。 |
  | %s                             | 输出一个字符串                                                                                                    |

# 输入

## 键盘输入

程序是人机交互的媒介，有输出必然也有输入，第三章我们讲解了如何将数据输出到显示器上，本章我们开始讲解如何从键盘输入数据。在C语言中，有多个函数可以从键盘获得用户输入：

* scanf()：和 printf() 类似，scanf() 可以输入多种类型的数据。

* getchar()、getche()、getch()：这三个函数都用于输入单个字符。

* gets()：获取一行数据，并作为字符串处理。

* scanf("%d %d", &a, &b);  // 获取用户输入的两个整数，分别赋值给变量 a 和 b

* printf("%d %d", a, b);  // 将变量 a 和 b 的值在显示器上输出
  
  | 格式控制符      | 说明                                                            |
  | ---------- | ------------------------------------------------------------- |
  | %c         | 读取一个单一的字符                                                     |
  | %hd、%d、%ld | 读取一个十进制整数，并分别赋值给 short、int、long 类型                            |
  | %ho、%o、%lo | 读取一个八进制整数（可带前缀也可不带），并分别赋值给 short、int、long 类型                  |
  | %hx、%x、%lx | 读取一个十六进制整数（可带前缀也可不带），并分别赋值给 short、int、long 类型                 |
  | %hu、%u、%lu | 读取一个无符号整数，并分别赋值给 unsigned short、unsigned int、unsigned long 类型 |
  | %f、%lf     | 读取一个十进制形式的小数，并分别赋值给 float、double 类型                           |
  | %e、%le     | 读取一个指数形式的小数，并分别赋值给 float、double 类型                            |
  | %g、%lg     | 既可以读取一个十进制形式的小数，也可以读取一个指数形式的小数，并分别赋值给 float、double 类型         |
  | %s         | 读取一个字符串（以空白符为结束）                                              |

### scanf高级用法

```
#include <stdio.h>


int main(){
    int n;
    float f;
    char str[23];
    scanf_s("%2d", &n);
    scanf_s("%*[^\n]"); scanf_s("%*c");  //清空缓冲区
    scanf_s("%5f", &f);
    scanf_s("%*[^\n]"); scanf_s("%*c");  //清空缓冲区
    scanf_s("%22s", str);
    printf("n=%d, f=%g, str=%s\n", n, f, str);
    return 0;
}

```

# 变量

## 基本变量类型

## 变量声明

**extern int i; //声明，不是定义**
**int i; //声明，也是定义**

* 1、一种是需要建立存储空间的。例如：int a 在声明的时候就已经建立了存储空间。
* 2、另一种是不需要建立存储空间的，通过使用extern关键字声明变量名而不定义它。 例如：extern int a 其中变量 a 可以在别的文件中定义的。
* 除非有extern关键字，否则都是变量的定义。

```
#include <stdio.h>

int x;
int y;

int addtwonum() {
    extern int x;
    extern int y;
    x = 1;
    y = 3;
    return x + y;
}


int main()
{

    int result= addtwonum();

    printf(" result 为  %d", result);

    return 0;

}
```

如果需要在一个源文件中引用另外一个源文件中定义的变量，我们只需在引用的文件中将变量加上 extern 关键字的声明即可。

定义addTwoNum

```
#include  <stdio.h>

extern int x;
extern  int y;

int addTwoNum() {

    return x + y;
}
```

定义demo.c

```
#include <stdio.h>

int x=6;
int y=9;


int addTwoNum();

int main()
{
    int result;
    result= addTwoNum();


    printf(" result 为  %d", result);

    return 0;

}
```

# 常量

const int A=1

# C 存储类

存储类定义 C 程序中变量/函数的的存储位置、生命周期和作用域。

## auto

**auto** 存储类是所有局部变量默认的存储类。

定义在函数中的变量默认为 auto 存储类，这意味着它们在函数开始时被创建，在函数结束时被销毁

## register

**register** 存储类用于定义存储在寄存器中而不是 RAM 中的局部变量。这意味着变量的最大尺寸等于寄存器的大小（通常是一个字），且不能对它应用一元的 '&' 运算符（因为它没有内存位置）。

register 存储类定义存储在寄存器，所以变量的访问速度更快，但是它不能直接取地址，因为它不是存储在 RAM 中的。在需要频繁访问的变量上使用 register 存储类可以提高程序的运行速度。

{
   register int  miles;
}

## static 存储类

**static** 存储类指示编译器在程序的生命周期内保持局部变量的存在，而不需要在每次它进入和离开作用域时进行创建和销毁。因此，使用 static 修饰局部变量可以在函数调用之间保持局部变量的值。

static 修饰符也可以应用于全局变量。当 static 修饰全局变量时，会使变量的作用域限制在声明它的文件内。

全局声明的一个 static 变量或方法可以被任何函数或方法调用，只要这些方法出现在跟 static 变量或方法同一个文件中。

静态变量在程序中只被初始化一次，即使函数被调用多次，该变量的值也不会重置。

```
#include <stdio.h>

/* 函数声明 */
void func1(void);

static int count=10;        /* 全局变量 - static 是默认的 */

int main()
{
  while (count--) {
      func1();
  }
  return 0;
}

void func1(void)
{
/* 'thingy' 是 'func1' 的局部变量 - 只初始化一次
 * 每次调用函数 'func1' 'thingy' 值不会被重置。
 */                
  static int thingy=5;
  thingy++;
  printf(" thingy 为 %d ， count 为 %d\n", thingy, count);
}
```

## extern 存储类

**extern** 存储类用于定义在其他文件中声明的全局变量或函数。当使用 extern 关键字时，不会为变量分配任何存储空间，而只是指示编译器该变量在其他文件中定义。

**extern** 存储类用于提供一个全局变量的引用，全局变量对所有的程序文件都是可见的。当您使用 **extern** 时，对于无法初始化的变量，会把变量名指向一个之前定义过的存储位置。

当您有多个文件且定义了一个可以在其他文件中使用的全局变量或函数时，可以在其他文件中使用 _extern_ 来得到已定义的变量或函数的引用。可以这么理解，_extern_ 是用来在另一个文件中声明一个全局变量或函数。

第一个文件

```
#include <stdio.h>

int count ;
extern void write_extern();

int main()
{
   count = 5;
   write_extern();
}
```

第二个文件

```
#include <stdio.h>

extern int count;

void write_extern(void)
{
   printf("count is %d\n", count);
}
```

# 函数

函数需要先声明再使用

```
#include <stdio.h>

/* 函数声明 */
int max(int num1, int num2);

int main ()
{
   /* 局部变量定义 */
   int a = 100;
   int b = 200;
   int ret;

   /* 调用函数来获取最大值 */
   ret = max(a, b);

   printf( "Max value is : %d\n", ret );

   return 0;
}

/* 函数返回两个数中较大的那个数 */
int max(int num1, int num2) 
{
   /* 局部变量声明 */
   int result;

   if (num1 > num2)
      result = num1;
   else
      result = num2;

   return result; 
}
```

### 指针变量作为函数参数

```
#include <stdio.h>
void swap(int *p1, int *p2){
    int temp;  //临时变量
    temp = *p1;
    *p1 = *p2;
    *p2 = temp;
}
int main(){
    int a = 66, b = 99;
    swap(&a, &b);
    printf("a = %d, b = %d\n", a, b);
    return 0;
}
```

调用 swap() 函数时，将变量 a、b 的地址分别赋值给 p1、p2，这样 *p1、*p2 代表的就是变量 a、b 本身，交换 *p1、*p2 的值也就是交换 a、b 的值。函数运行结束后虽然会将 p1、p2 销毁，但它对外部 a、b 造成的影响是“持久化”的，不会随着函数的结束而“恢复原样”。  

需要注意的是临时变量 temp，它的作用特别重要，因为执行`*p1 = *p2;`语句后 a 的值会被 b 的值覆盖，如果不先将 a 的值保存起来以后就找不到了。



```
#include <stdio.h>
int max(int *intArr, int len){
    int i, maxValue = intArr[0];  //假设第0个元素是最大值
    for(i=1; i<len; i++){
        if(maxValue < intArr[i]){
            maxValue = intArr[i];
        }
    }

    return maxValue;
}
int main(){
    int nums[6], i;
    int len = sizeof(nums)/sizeof(int);
    //读取用户输入的数据并赋值给数组元素
    for(i=0; i<len; i++){
        scanf("%d", nums+i);
    }
    printf("Max value is %d!\n", max(nums, len));
    return 0;
}
```



C语言为什么不允许直接传递数组的所有元素，而必须传递数组指针呢？  

参数的传递本质上是一次赋值的过程，赋值就是对内存进行拷贝。所谓内存拷贝，是指将一块内存上的数据复制到另一块内存上。  

对于像 int、float、char 等基本类型的数据，它们占用的内存往往只有几个字节，对它们进行内存拷贝非常快速。而数组是一系列数据的集合，数据的数量没有限制，可能很少，也可能成千上万，对它们进行内存拷贝有可能是一个漫长的过程，会严重拖慢程序的效率，为了防止技艺不佳的程序员写出低效的代码，C语言没有从语法上支持数据集合的直接赋值。

# 数组

## 二维数组

int a[5][3] = { {85,54,68} ,{85,54,68} ,{85,54,68} ,{85,54,68} ,{85,54,68} };

## 声明数组

int  num[]={1,2,3,45,5}

## 遍历数组

```
#include <stdio.h>



int main() {
    int array[10];
    int  i, j;
    for (i = 0;i < 10;i++) {
        array[i] = i + 100;
    }

    for (j = 0;j < 10;j++) {

        printf("Element[%d] = %d\n", j, array[j]);
    }

    return  0;
}
```

数组字符串的末尾在不指定长度的情况下会自动添加 0  比如char a[]="asasas"

如果指定了长度 你需要给 数组末尾添加0 或者 '\0'

## 字符串数组和字符数组的区别

在C语言中，字符数组和字符串数组都是用来存储一串字符的数据结构，但它们有以下区别：

1. 定义方式不同：字符数组的定义方式为char array_name[length];，其中length表示数组的长度；而字符串数组的定义方式为char array_name[length] = "string";，其中string表示字符串的内容，length表示数组的长度。

2. 存储方式不同：字符数组只能存储一串字符，而字符串数组可以存储多个字符串，每个字符串以'\0'（空字符）作为结尾。

3. 初始化方式不同：字符数组可以通过遍历赋值的方式进行初始化，也可以使用字符串常量进行初始化；而字符串数组只能通过字符串常量进行初始化。

4. 使用方式不同：字符数组可以通过下标访问每个字符，也可以将其作为函数的参数进行传递；而字符串数组通常用于存储多个字符串，并且可以使用字符串处理函数（如strcpy、strcat等）对字符串进行操作。

需要注意的是，字符串实际上是以'\0'结尾的字符数组，因此字符串数组也可以被看作是一种特殊的字符数组。

## 数组什么时候会转换为指针

 int a[] = {1, 2, 3, 4, 5}, * p, i = 2;
    //读者可以通过以下任何一种方式来访问 a[i]：

p = a;         
p[i];   

p = a;  
*(p + i);

p = a + i;  
*p;

这三种都可以

对数组的引用 a[i] 在编译时总是被编译器改写成`*(a+i)`的形式，C语言标准也要求编译器必须具备这种行为。  

取下标操作符`[ ]`是建立在指针的基础上，它的作用是使一个指针和一个整数相加，产生出一个新的指针，然后从这个新指针（新地址）上取得数据；假设指针的类型为`T *`，所产生的结果的类型就是`T`。  

取下标操作符的两个操作数是可以交换的，它并不在意操作数的先后顺序，就像在加法中 3+5 和 5+3 并没有什么不一样。以上面的数组 a 为例，如果希望访问第 3 个元素，那么可以写作`a[3]`，也可以写作`3[a]`，这两种形式都是正确的，只不过后面的形式从不曾使用，它除了可以把初学者搞晕之外，实在没有什么实际的意义。

> a[3] 等价于 *(a + 3)，3[a] 等价于 *(3 + a)，仅仅是把加法的两个操作数调换了位置。



# 枚举

枚举是 C 语言中的一种基本数据类型，用于定义一组具有离散值的常量。，它可以让数据更简洁，更易读。

枚举类型通常用于为程序中的一组相关的常量取名字，以便于程序的可读性和维护性。

定义一个枚举类型，需要使用 enum 关键字，后面跟着枚举类型的名称，以及用大括号 {} 括起来的一组枚举常量。每个枚举常量可以用一个标识符来表示，也可以为它们指定一个整数值，如果没有指定，那么默认从 0 开始递增。

**1、先定义枚举类型，再定义枚举变量**

```
enum DAY
{
      MON=1, TUE, WED, THU, FRI, SAT, SUN
};
enum DAY day;
```

**2、定义枚举类型的同时定义枚举变量**

```
enum DAY
{
      MON=1, TUE, WED, THU, FRI, SAT, SUN
} day;
```

**3、省略枚举名称，直接定义枚举变量**

```
enum
{
      MON=1, TUE, WED, THU, FRI, SAT, SUN
} day;
```

4 实例

```
#include <stdio.h>

enum DAY
{
      MON=1, TUE, WED, THU, FRI, SAT, SUN
} day;
int main()
{
    // 遍历枚举元素
    for (day = MON; day <= SUN; day++) {
        printf("枚举元素：%d \n", day);
    }
}
```

# 指针

## 空指针



## 二级指针

二级指针就是指向指针的指针

1. int a =100;

2. int *p1 = &a;

3. int **p2 = &p1;
   二级指针指向一级指针 ，三级指针指向二级指针

## 定义指针变量

定义指针变量与定义普通变量非常类似，不过要在变量名前面加星号`*`，格式为：





正如您所知道的，每一个变量都有一个内存位置，每一个内存位置都定义了可使用 & 运算符访问的地址，它表示了在内存中的一个地址。

```
int main() {
    int a = 100, b = 10;
    int* p1,* p2;
    p1 = &a;
    p2 = &b;

    printf("a=%d,b=%d\n", a, b);
    printf("*p1 =%d ,*p2=%d\n", *p1, *p2);

    return  0;
}
```

指针变量 p 和 *p 有什么区别  p代表的时候地址值   *p代表的是地址值存放的内容

指针也就是内存地址，指针变量是用来存放内存地址的变量。就像其他变量或常量一样，您必须在使用指针存储其他变量地址之前，对其进行声明。指针变量声明的一般形式为：

## 数组指针(指向数组的指针)

**<u>所谓数组元素的指针就是数组元素的地址</u>**

**<u>可以用一个指针变量指向一个数组元素。例如</u>**

```
#include <stdio.h>



int main() {


    int arr[10] = { 1,2,3,4,5,6,4,5,4,8 };
    int* p = &arr[0];



    printf("*p=%d\n", p);

    return  0;
}
```

### &数组名vs数组名

```
 根据代码我们发现，其实&arr和arr，虽然值是一样的，但是意义应该不一样

实际上&arr表示的是数组的地址而不是数组首元素的地址。

数组的地址+1 跳过整个数组的大小，所以&arr+1相对于&arr的差值是40. 

 #include <stdio.h>



int main() {


    int arr[10] = { 1,2,3,4,5,6,4,5,4,8 };
    int* p = &arr[1];



    printf("*p=%d\n", arr);//这个是首元素的地址
    printf("*p=%d\n", arr+1);//这个是首元素地址加1
    printf("*p=%d\n", p); //这个是数组中第二个元素的地址
    printf("*p=%d\n", &arr);//这个是数组的地址

    return  0;
}
```

首元素地址加1指向下一个元素的地址  数组的地址值加1 代表下个数组 比如数组的长度是10 int类型每个是4个字节 那就是40

## 野指针

局部变量指针未初始化，默认为随机变量

## 函数指针

函数指针就是指向函数的指针

使用

```
#include <stdio.h>

int  max(int x, int y) {

    return x > y ? x : y;
}




int main() {

    int (*p)(int, int) = &max;

    int a = 1, b = 2, c = 3 ,d;

    d = p(p(a, b), c);

    printf("最大的数 : %d\n", d);



    return  0;
}
```

### 指针作为函数返回值

C语言允许函数的返回值是一个[指针](http://c.biancheng.net/c/80/)（地址），我们将这样的函数称为指针函数。下面的例子定义了一个函数 strlong()，用来返回两个字符串中较长的一个：



```
#include <stdio.h>

#include <string.h>

char* strlong(char* str1, char* str2) {
    if (strlen(str1) >= strlen(str2)) {

        return str1;
    }
    else {
        return str2;
    }

}

int main() {
    char str1[30], str2[30], * str;
    gets(str1);
    gets(str2);
    str = strlong(str1, str2);
    printf("%s", str);

    return 0;
}
```

用指针作为函数返回值时需要注意的一点是，函数运行结束后会销毁在它内部定义的所有局部数据，包括局部变量、局部数组和形式参数，函数返回的指针请尽量不要指向这些数据，C语言没有任何机制来保证这些数据会一直有效，它们在后续使用过程中可能会引发运行时错误。请看下面的例子：

## 数组指针

如果一个数组中的所有元素保存的都是[指针](http://c.biancheng.net/c/80/)，那么我们就称它为指针数组。指针数组的定义形式一般为：

```
#include <stdio.h>

const int MAX = 3;

int main ()
{
   int  var[] = {10, 100, 200};
   int i, *ptr[MAX];

   for ( i = 0; i < MAX; i++)
   {
      ptr[i] = &var[i]; /* 赋值为整数的地址 */
   }
   for ( i = 0; i < MAX; i++)
   {
      printf("Value of var[%d] = %d\n", i, *ptr[i] );
   }
   return 0;
}
```

## 数组灵活多变的访问方式

```
#include <stdio.h>
int main(){
    char str[20] = "c.biancheng.net";

    char *s1 = str;
    char *s2 = str+2;

    char c1 = str[4];
    char c2 = *str;
    char c3 = *(str+4);
    char c4 = *str+2;
    char c5 = (str+1)[5];

    int num1 = *str+2;
    long num2 = (long)str;
    long num3 = (long)(str+2);
    printf("  s1 = %s\n", s1);
    printf("  s2 = %s\n", s2);
    printf("  c1 = %c\n", c1);
    printf("  c2 = %c\n", c2);
    printf("  c3 = %c\n", c3);
    printf("  c4 = %c\n", c4);
    printf("  c5 = %c\n", c5);

    printf("num1 = %d\n", num1);
    printf("num2 = %ld\n", num2);
    printf("num3 = %ld\n", num3);
    return 0;
}
 s1 = c.biancheng.net
  s2 = biancheng.net
  c1 = a
  c2 = c
  c3 = a
  c4 = e
  c5 = c
num1 = 101
num2 = 2686736
num3 = 2686738
```

```
 1) str 既是数组名称，也是一个指向字符串的指针；指针可以参加运算，加 1 相当于数组下标加 1。
printf() 输出字符串时，要求给出一个起始地址，并从这个地址开始输出，直到遇见字符串结束标志\0。s1 为字符串 str 第 0 个字符的地址，s2 为第 2 个字符的地址，所以 printf() 的结果分别为 c.biancheng.net 和 biancheng.net。

2) 指针可以参加运算，str+4 表示第 4 个字符的地址，c3 = *(str+4) 表示第4个字符，即 'a'。

3) 其实，数组元素的访问形式可以看做 address[offset]，address 为起始地址，offset 为偏移量：c1 = str[4]表示以地址 str 为起点，向后偏移4个字符，为 'a'；c5 = (str+1)[5]表示以地址 str+1 为起点，向后偏移5个字符，等价于str[6]，为 'c'。

4) 字符与整数运算时，先转换为整数（字符对应的ASCII码）。num1 与 c4 右边的表达式相同，对于 num1，*str+2 == 'c'+2 == 99+2 == 101，即 num1 的值为 101，对于 c4，101 对应的字符为 ‘e’，所以 c4 的输出值为 'e'。

5) num2 和 num3 分别为字符串 str 的首地址和第 2 个元素的地址。
```

## 回调函数

回调函数就是一个通过函数指针调用的函数。如果你把函数的指针（地址）作为参数传递给另一个函数，当这个指针被用来调用其所指向的函数时，我们就说这是回调函数。回调函数不是由该函数的实现方直接调用，而是在特定的事件或条件发生时由另外的一方调用的，用于对该事件或条件进行响应。

```
#include <stdio.h>

int  max(int x, int y) {

    return x > y ? x : y;
}

int ll(int (*p)(int, int), int x, int y) {

    return p(x, y);
}


int main() {


    int a = 1, b = 2, c = 3 ,d;

    d = ll(&max, a, b);

    printf("最大的数 : %d\n", d);



    return  0;
}
```

# 字符串指针

1. char *str = "http://c.biancheng.net";

字符串中的所有字符在内存中是连续排列的，str 指向的是字符串的第 0 个字符；我们通常将第 0  个字符的地址称为字符串的首地址。字符串中每个字符的类型都是`char`，所以 str 的类型也必须是`char *`。

char  * str="hello word";

printf("%p", str);

||  printf("%p",(str+0))



```
#include <stdio.h>
#include <string.h>
int main(){
    char *str = "http://c.biancheng.net";
    int len = strlen(str), i;

    //直接输出字符串
    printf("%s\n", str);
    //使用*(str+i)
    for(i=0; i<len; i++){
        printf("%c", *(str+i));
    }
    printf("\n");
    //使用str[i]
    for(i=0; i<len; i++){
        printf("%c", str[i]);
    }
    printf("\n");
    return 0;
}
```

字符串指针输出的时候 用的是 %s 输出的是字符串 用的是%p输出的是地址值 (str+i) 等价于 str[i],但是读取地址值 必须要用 (str+i)

字符串指针和字符串数组的区别

字符串指针只能读取不能写入

比如

```
#include <stdio.h>
int main(){
    char *str = "Hello World!";
    str = "I love C!";  //正确
    str[3] = 'P';  //错误
    return 0;
}
```

### 到底使用字符数组还是字符串常量

在编程过程中如果只涉及到对字符串的读取，那么字符数组和字符串常量都能够满足要求；如果有写入（修改）操作，那么只能使用字符数组，不能使用字符串常量。

## 指针总结

| 定  义         | 含  义                                          |
| ------------ | --------------------------------------------- |
| int *p;      | p 可以指向 int 类型的数据，也可以指向类似 int arr[n] 的数组。      |
| int **p;     | p 为二级指针，指向 int * 类型的数据。                       |
| int *p[n];   | p 为指针数组。[ ] 的优先级高于 *，所以应该理解为 int *(p[n]);     |
| int (*p)[n]; | p 为[二维数组](http://c.biancheng.net/c/array/)指针。 |
| int *p();    | p 是一个函数，它的返回值类型为 int *。                       |
| int (*p)();  | p 是一个函数指针，指向原型为 int func() 的函数。               |

# 字符串

在 C 语言中，字符串实际上是使用空字符 \0 结尾的一维字符数组。因此，\0 是用于标记字符串的结束。

**空字符（Null character**）又称结束符，缩写 NUL，是一个数值为 0 的控制字符，\0 是转义字符，意思是告诉编译器，这不是字符 0，而是空字符。

下面的声明和初始化创建了一个 **RUNOOB** 字符串。由于在数组的末尾存储了空字符 \0，所以字符数组的大小比单词 **RUNOOB** 的字符数多一个。

```
int main() {

    char site[] = "hello";

    printf("菜鸟教程: %s\n", site);
    return  0;
}


```

在C语言的char型数组中，数字0（和字符‘\0’等价）结尾的[char数组](https://so.csdn.net/so/search?q=char%E6%95%B0%E7%BB%84&spm=1001.2101.3001.7020)就是一个字符串，但如果char型数组没有以数字0结尾，那么就不是一个字符串，只是普通字符数组，所以字符串是一种特殊的char的数组。

另外一种定义字符串的方式

char *str = "http://c.biancheng.net";

## 字符串输入

在C语言中，有两个函数可以让用户从键盘上输入字符串，它们分别是：

* scanf()：通过格式控制符`%s`输入字符串。除了字符串，scanf() 还能输入其他类型的数据。
* gets()：直接输入字符串，并且只能输入字符串。
  
  

### 字符串拼接

```
#include <stdio.h>
#include <string.h>
int main(){
    char str1[100]="The URL is ";
    char str2[60];
    printf("Input a URL: ");
    gets(str2);
    strcat(str1, str2);
    puts(str1);

    return 0;
}
```

strcat(arrayName1, arrayName2);

strcat() 将把 arrayName2 连接到 arrayName1 后面，并删除原来 arrayName1 最后的结束标志`'\0'`。这意味着，arrayName1 必须足够长，要能够同时容纳 arrayName1 和 arrayName2，否则会越界（超出范围）。

### 字符串复制函数 strcpy()

strcpy 是 string copy 的缩写，意思是字符串复制，也即将字符串从一个地方复制到另外一个地方，语法格式为：

strcpy(arrayName1, arrayName2);

strcpy() 会把 arrayName2 中的字符串拷贝到 arrayName1 中，字符串结束标志`'\0'`也一同拷贝。请看下面的例子：



```
#include <stdio.h>
#include <string.h>  //记得引入该头文件


int main(){
    char str[100] = "this Url";
    char str2[50] = "kkkkkkkkk";
    gets(str2);
    strcpy(str, str2);
    printf("%s", str);




    return 0;
}


```

### 字符串比较函数 strcmp()

strcmp 是 string compare 的缩写，意思是字符串比较，语法格式为：

strcmp(arrayName1, arrayName2);

```
#include <stdio.h>
#include <string.h>
int main(){
    char a[] = "aBcDeF";
    char b[] = "AbCdEf";
    char c[] = "aacdef";
    char d[] = "aBcDeF";
    printf("a VS b: %d\n", strcmp(a, b));
    printf("a VS c: %d\n", strcmp(a, c));
    printf("a VS d: %d\n", strcmp(a, d));

    return 0;
}
```

# 结构体

因为在实际问题中，一组数据往往有很多种不同的数据类型。例如，登记学生的信息，可能需要用到 char型的姓名，int型或 char型的学号，int型的年龄，char型的性别，float型的成绩。又例如，对于记录一本书，需要 char型的书名，char型的作者名，float型的价格。在这些情况下，使用简单的基本数据类型甚至是数组都是很困难的。而结构体（类似Pascal中的“记录”），则可以有效的解决这个问题。

定义结构体

```
struct Student
{
    char name[20];
    int age;
};
```

定义结构体变量

struct Student stu1;

结构体变量的定义也可以与结构体的声明同时，这样就简化了代码：

```
struct Student
{
    char name[20];
    int age;
}stu1;
```

还可以使用匿名结构体来定义结构体变量：

struct { //没有结构名

char name[20];

int num;

float score;

}stu1;

但要注意的是这样的方式虽然简单，但不能再次定义新的结构体变量了。

```
#include <stdio.h>



struct Student
{
    char name[20];
    int age;
}stu1;

void mxdx(struct Student  stu1) {
    printf("llll  %d\n", stu1.age);
}

int main() {
    struct Student    stu1 = { "hello",15 };
    mxdx(stu1);
    printf("结构体 %d\n", stu1);
}


```

## 结构体数组

```
#include <stdio.h>





int main() {
    struct MyStruct
    {
        int name;
        int age;
    }class[3] = {

        {"张三",15},
        {"李四",25}


    };
        
        
        
    printf("%d", class[0].age);



    return 0;
}
```

## 结构体指针

struct 结构体名 *变量名;

下面是一个定义结构体指针的实例：

```
//结构体
struct stu{
    char *name;  //姓名
    int num;  //学号
    int age;  //年龄
    char group;  //所在小组
    float score;  //成绩
} stu1 = { "Tom", 12, 18, 'A', 136.5 };
//结构体指针
struct stu *pstu = &stu1;
```

也可以在定义结构体的同时定义结构体指针：

```
struct stu{
    char *name;  //姓名
    int num;  //学号
    int age;  //年龄
    char group;  //所在小组
    float score;  //成绩
} stu1 = { "Tom", 12, 18, 'A', 136.5 }, *pstu = &stu1;
```

### 获取结构体成员

(*pointer).memberName

pointer->memberName

#### 结构体数组指针

```
#include <stdio.h>


struct stu {
    char* name;  //姓名
    int num;  //学号
    int age;  //年龄
    char group;  //所在小组
    float score;  //成绩
}stus[] = {
    {"Zhou ping", 5, 18, 'C', 145.0},
    {"Zhang ping", 4, 19, 'A', 130.5},
    {"Liu fang", 1, 18, 'A', 148.5},
    {"Cheng ling", 2, 17, 'F', 139.0},
    {"Wang ming", 3, 17, 'B', 144.5}
}, * ps;


int main() {
    int len = sizeof(stus) / sizeof(struct stu);
    printf("Name\t\tNum\tAge\tGroup\tScore\t\n");
    for (ps = stus; ps < stus + len; ps++) {
        printf("%s\t%d\t%d\t%c\t%.1f\n", ps->name, ps->num, ps->age, ps->group, ps->score);
    }

    



    return 0;
}
```

#### 结构体指针作为函数参数

结构体变量名代表的是整个集合本身，作为函数参数时传递的整个集合，也就是所有成员，而不是像数组一样被编译器转换成一个指针。如果结构体成员较多，尤其是成员为数组时，传送的时间和空间开销会很大，影响程序的运行效率。所以最好的办法就是使用结构体指针，这时由实参传向形参的只是一个地址，非常快速。

```
#include <stdio.h>
struct stu{
    char *name;  //姓名
    int num;  //学号
    int age;  //年龄
    char group;  //所在小组
    float score;  //成绩
}stus[] = {
    {"Li ping", 5, 18, 'C', 145.0},
    {"Zhang ping", 4, 19, 'A', 130.5},
    {"He fang", 1, 18, 'A', 148.5},
    {"Cheng ling", 2, 17, 'F', 139.0},
    {"Wang ming", 3, 17, 'B', 144.5}
};
void average(struct stu *ps, int len);
int main(){
    int len = sizeof(stus) / sizeof(struct stu);
    average(stus, len);
    return 0;
}
void average(struct stu *ps, int len){
    int i, num_140 = 0;
    float average, sum = 0;
    for(i=0; i<len; i++){
        sum += (ps + i) -> score;
        if((ps + i)->score < 140) num_140++;
    }
    printf("sum=%.2f\naverage=%.2f\nnum_140=%d\n", sum, sum/5, num_140);
}
```

# 共用体

**共用体**是一种特殊的数据类型，允许您在相同的内存位置存储不同的数据类型。您可以定义一个带有多成员的共用体，但是任何时候只能有一个成员带有值。共用体提供了一种使用相同的内存位置的有效方式。

```
#include <stdio.h>


union Data {

    int i;
    float f;
    char str[20];
} data;


struct Student
{
    char name[20];
    int age;
}stu1;

void mxdx(struct Student  stu1) {
    printf("llll  %d\n", stu1.age);
}

int main() {

    union Data data;
    data.i = 50;


    printf("data.i = %d\n", data.i);

}



```

看下面例子，可以知道，共用体只能存储一个值，只能保存最后一个赋值的成员的值
C 位域
====

```
union Data {

    int i;
    float f;
    char str[20];
} data;


struct Student
{
    unsigned int num1 : 4;// 一个位 二进制未1 也就只能表示 0 或者 1
    char name[20];
    int age;
}stu1;

void mxdx(struct Student  stu1) {
    printf("llll  %d\n", stu1.age);
}

int main() {
    stu1.num1 = 2;
    union Data data;
    data.i = 50;
    data.f = 50.5;

    printf("data.i = %d\n", stu1.num1);

}

```

# typedef

C语言允许为一个数据类型起一个新的别名，就像给人起“绰号”一样。  

起别名的目的不是为了提高程序运行效率，而是为了编码方便。例如有一个结构体的名字是 stu，要想定义一个结构体变量就得这样写：



#include <stdio.h>





typedef  struct Student
{
    unsigned int num1 : 4;// 一个位 二进制未1 也就只能表示 0 或者 1
    char name[20];
    int age;
}Student;



int main() {
    Student student;
    student.age = 15;
    return 0;

}

# 文件读写

C把文件看作是一系列的连续的字节，每个字节都能被单独读取。

C提供两种文件模式：文本模式和二进制模式

文本内容：如果文件最初使用的是二进制编码的字符（如ASCII码）表示文本，该文件就是                      文本文件，其中包含文本内容。
二进制内容：如果文件中的二进制值代表机器语言代码或数值数据，图片或者音乐编码，该                       文件就是二进制文件，其中包含二进制内容。

在C语言中，操作文件之前必须先打开文件；所谓“打开文件”，就是让程序和文件建立连接的过程。  

打开文件之后，程序可以得到文件的相关信息，例如大小、类型、权限、创建者、更新时间等。在后续读写文件的过程中，程序还可以记录当前读写到了哪个位置，下次可以在此基础上继续操作

> 标准输入文件 stdin（表示键盘）、标准输出文件 stdout（表示显示器）、标准错误文件 stderr（表示显示器）是由系统打开的，可直接使用。

fopen() 会获取文件信息，包括文件名、文件状态、当前读写位置等，并将这些信息保存到一个 FILE 类型的结构体变量中，然后将该变量的地址返回。  

FILE 是 <stdio.h> 头文件中的一个结构体，它专门用来保存文件信息。我们不用关心 FILE 的具体结构，只需要知道它的用法就行。  

如果希望接收 fopen() 的返回值，就需要定义一个 FILE 类型的[指针](http://m.biancheng.net/c/80/)。例如：

FILE * pf=fopen("D:\\xiexiake\\demo.txt","r");
