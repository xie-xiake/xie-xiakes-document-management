# 为什么学习CSS，因为这是基础

### PxCook

像素测量工具

### 块级元素

默认独占一行 比如 div p  h系列

默认宽度是父级的100%

### 行内元素

代表标签 a span b u i s

一行可以显示多个

宽度和高度默认由内容撑开

不可以设置宽高

### 行内块元素

一行可以显示多个

可以设置宽高

代表标签  input  textarea  button select

### HTML嵌套规范注意点

块级元素一般作为大容器，可以嵌套

### CSS继承

子元素默认继承父元素样式的特点，但是只能继承 控制文字的样式，比如color  文字样式 首行缩进

### 标签的转换

```
display: inline-block;//将块元素转换为行内块元素，这样两个div就可以在一起并排显示
display: inline;//将块元素转换为行内元素，行内元素的特点是一行多个 不可以设置宽高
display: block; 将行内元素转换为块元素

```

### 图片标签

```
<img src="/4f992c36d73aa8f9189ae20dda73646.jpg" alt="替换文本"  title="某某图片"  width="200" height="1200">
alt:图片加载失败后的替换文本
title: 鼠标移动到图片上的提示文本
width: 款 height: 高 如果两个值同时设定图片会出问题

```

### 音频标签

```
 <audio src="/4f992c36d73aa8f9189ae20dda73646.jpg" controls autoplay loop></audio>
 controls: 显示播放组件
 autoplay：刷新页面自动播放 部分浏览器不支持
 loop: 循环播放
 音频标签目前支持三种格式: MP3  Wav  Ogg
```

### 视频标签

```
<video src="" controls autoplay loop></video>

 controls: 显示播放组件
 autoplay：刷新页面自动播放 部分浏览器不支持
 loop: 循环播放
```

### 链接标签

```
<a href="http://www.baidu.com" target="_blank">点击跳转</a>

href: 跳转地址
target="_blank" 跳转网页打开新页面
```

### 引入外部css

```
<link rel="stylesheet"  type="text/css" href="a.css">
```

### 标题和段落标签

<h1></h1> 这是标题标签

p标签是段落标签

段落与段落之前是有空行的

### 换行和水平线标签

换行标签是 <br>

水平线标签是<hr>

### 文本格式化标签

| 标签 | 说明   |
| ---- | ------ |
| b    | 加粗   |
| u    | 下划线 |
| i    | 倾斜   |
| s    | 删除线 |

亦可以用这种写法

| 标签   | 说明   |
| ------ | ------ |
| strong | 加粗   |
| ins    | 下划线 |
| em     | 倾斜   |
| del    | 删除线 |

### 无序列表

```
 list-style: none;去除黑点
    <ul>
        <li>榴莲</li>
        <li>香蕉</li>

    </ul>
```

### 有序列表

```
    <ol>
        <li>榴莲</li>
        <li>香蕉</li>
        <li>香蕉</li>
        <li>香蕉</li>
        <li>香蕉</li>

    </ol>
```

### 自定义列表

```
  <dl>
      <dt>主题</dt>
      <dd>内容</dd>
      <dd>内容</dd>
      <dd>内容</dd>

  </dl>
```

### 表格

```
  <table border="1" width="600" height="400" >
  <caption>ccc</caption>
        <tr>

            <th>
                姓名
            </th>
            <th>
                成绩
            </th>
            <th>
                评估
            </th>
        </tr>

       <tr>
           <td>张三</td>
           <td>100</td>
           <td>优秀</td>
       </tr>
       <tr>
           <td>张2三</td>
           <td>100</td>
           <td>优秀</td>
       </tr>


   </table>
   
   table>tr>td&th   
   th是表头
   border是表格边框线
   caption 是表格标题
```

### 结构表格

```
   <table border="1" width="600" height="400" >
  <caption>ccc</caption>
       <thead>
       <tr>

           <th>
               姓名
           </th>
           <th>
               成绩
           </th>
           <th>
               评估
           </th>
       </tr>

       </thead>
       <tbody>
       <tr>
           <td>张三</td>
           <td>100</td>
           <td>优秀</td>
       </tr>
       <tr>
           <td>张2三</td>
           <td>100</td>
           <td>优秀</td>
       </tr>


       </tbody>
   <tfoot>
   <tr>
       <td>总结</td>
       <td>总结</td>
       <td>总结</td>
   </tr>
 


   </tfoot>



   </table>
  thead: 表头
  tbody: 表身
  tfoot: 表尾
  
  比传统表格效率更高
```

### 合并单元格

```

   <table border="1" width="600" height="400" >
  <caption>ccc</caption>
       <thead>
       <tr>

           <th>
               姓名
           </th>
           <th>
               成绩
           </th>
           <th>
               评估
           </th>
       </tr>

       </thead>
       <tbody>
       <tr>
           <td>张三</td>
           <td rowspan="2">100</td>
           <td>优秀</td>
       </tr>
       <tr>
           <td>张2三</td>

           <td>优秀</td>
       </tr>


       </tbody>
   <tfoot>
   <tr>
       <td>总结</td>
       <td colspan="2">总结</td>
   </tr>



   </tfoot>



   </table>
rowspan:"2" 跨两行合并
colspan="2" 跨两列合并
不能跨结构合并
```

### input框

```
<!--        文本框-->
     <input type="text" placeholder="请输入用户名">
<!--    密码框-->
    <input type="password">
   <br>
<!--    单选框-->
    <input type="radio" name="sex">男
    <input type="radio" name="sex" checked>女


    <br>
<!--    多选框-->
    <input  type="checkbox">

<!--    上传文件 MULTIPLE上传多个文件-->
    <input type="file" MULTIPLE>
```

### input按钮

```
    
    <input type="submit">
    <input type="reset">
    <input type="button" value="">
    submit 和reset 一个是提交一个是清除 配合表单域标签 form
    button是普通按钮
    value: 是按钮名称
```

### 下拉菜单

```
    <select>
        <option>aa</option>
        <option  selected>bb</option>
        <option>cc</option>
        <option>dd</option>



    </select>
  selected: 默认选中  
```

### 文本域

```
   <textarea cols="20"  rows="30">
   这里面的文本会自动换行

   </textarea>
   cols: 规定文本域可见宽度
   rows: 规定了文本域可见行数
```

### label标签

label标签的作用是点中文本也可以选中组件

label标签的两种使用方法

```
  <input type="radio" name="sex" id="nan"><label for="nan">男</label>
  
  推荐用下面这种
    <label>    <input type="radio" name="sex" checked>女
    </label>
```

### 语义化标签

做移动端使用

```
   <header>网页头部</header>
    <nav>网页导航</nav>
    <footer>网页底部</footer>
    <aside>网页侧边栏</aside>
    <section>网页区块</section>
    <article>文章</article>
```

### 字符实体

空格: &nbsp;

### 颜色设置

1. 使用名称 color: red;

2. 使用16进制 color: #f03;

3. 通过 rgb() 函数对 red green  blue 三原色强度进行控制 实现不同的颜色 color: rgb(255,0,54);

4. rgba扩展了rgb，在rgb的基础上增加了透明度color: rgba(255.0.0.0.1)

5. 通过 hsl0 函数对颜色的色调、饱和度、亮度进行调节，从而实现不同的颜色color: hsl(120,100%,25%):

6. HSLA 扩展了 HSL，在HSL的基础上增加了 alpha 通道来设置颜色的透明度，需要使用 hsla0 函数实现 color:
   hsla (240,100% 50%,0背木台

### 盒子模型

内容(content) 也就是元素的(width,height)

内边距(padding) 内容和边框之间的距离 上右下左，设置padding的左右距离，很重要 比如说 一个4个字的内容和一个5个字的内容是完全不一样的

边框(border) 边框线的粗细

外边距(margin) 和旁边标签的距离

影响盒模型的大小：内容、内边距、边框，跟外边框没有关系。

```
.box1 {
   内容区的宽度
    height: 200px;
    内容区的高度
    width: 200px;
    内容区的背景颜色
    background-color: #5bc0de;
    内容区的颜色
    color: #1866d7;
    内容区的边框线条
    border: 10px  solid red;
    
    内容区到边框的长度
    padding: 50px;
    边框到外边的长度
    margin: 25px;
}
```

边框线

```
上边框线
border-top: 3px solid #f0ad4e;
下边框线
border-bottom: 1px solid #5bc0de;
```

一个盒子真正的大小是 margin+border+padding+内容 组成的 

margin+border+padding 会撑大盒子 

### 盒子内减模式

margin+border+padding 会撑大盒子

如果希望盒子的原始 宽高不变

需要设置  ，他会帮助你自动减去多余的部分

```
box-sizing: border-box;
```

### 清除默认样式

```
*{
    margin: 0;
    padding: 0;
}
```

### 版心居中

```
margin: 0 auto;
```

### 盒子外边距折叠

场景: 垂直布局 的 块级元素  上下的margin会合并

合并的margin 是其中的最大值

解决方法: 只需要给其中ige盒子设置margin即可

### 盒子塌陷

互相嵌套的块级元素，子元素的 margin-top 会作用在父元素上

导致父元素一起往下移动

解决方法:

1. 给父元素设置border-top 或者 padding-top

2. 给父元素设置overflow: hidden

3. 转换为行内块元素

4. 设置浮动

   ### 行内标签怎么设置垂直

   line-height: 100px;

### 盒子定位

### 相对定位

相对定位就是元素相对于自己默认的位置来进行位置上的调整，可以通过 top、bottom、left 和 right 四个属性的组合来设置元素相对于默认位置在不同方向上的偏移量。

特点: 1 占有原来的位置

​        2 仍然具体标签原有的显示模式特点   

​      3  改变位置是参照自己

只能写 left 和 top

```
position: relative;
left: 100px;
top: 80px;
```

注意：相对定位的元素可以移动并与其他元素重叠，但会保留元素默认位置处的空间。

### 固定定位

固定定位就是将元素相对于浏览器窗口进行定位，使用固定定位的元素不会因为浏览器窗口的滚动而移动，就像是固定在了页面上一样，我们经常在网页上看到的返回顶部按钮就是使用固定定位实现的。

语法: position: fixed;            

### 绝对定位

先找已经定位的父级，如果有 参考他

如果父级没有定位，以浏览器窗口为参考物进行定位



绝对定位就是元素相对于第一个非静态定位（static）的父级元素进行定位，如果找不到符合条件的父级元素则会相对与浏览器窗口来进行定位。您同样可以使用 top、bottom、left 和 right 四个属性来设置元素相对于父元素或浏览器窗口不同方向上的偏移量。

特点

1 脱标 不占位置

2 改变显示模式

3 绝对定位的盒子不能使用margin  0 auto 的效果去居中 

 position: absolute;

### 装饰

# z-index：元素堆叠

每个元素都有一个默认的 z-index 属性，将 z-index 属性与 position 属性相结合可以创建出类似 PhotoShop 中的图层效果。z-index 属性可以设置元素的层叠级别（当元素出现重叠时，该元素在其它元素之上还是之下），拥有更高层叠级别的元素会处于层叠级别较低的元素的前面（或者说上面）。

通过 z-index 属性您可以创建更加复杂的网页布局，z-index 属性的可选值如下表所示：

| 值      | 描述                                   |
| ------- | -------------------------------------- |
| auto    | 默认值，堆叠顺序与父元素相等           |
| number  | 使用具体数值（整数）设置元素的堆叠顺序 |
| inherit | 从父元素继承 z-index 属性的值          |

关于元素的层级关系有以下几点需要注意：

- 对于未设置 position 属性的元素或者 position 属性的值为 static 时，后定义的元素会覆盖前面的元素；
- 对于设置有 position 属性且属性值不为 static 的元素，这些元素会覆盖没有设置 position 属性或者 position 属性值为 static 的元素；
- 对于 position 属性值不为 static 且定义了 z-index 属性的元素，z-index 属性值大的元素会覆盖 z-index 属性值小的元素，即 z-index 属性值越大优先级越高，若 z-index 属性值相同，则后定义的元素会覆盖前面定义的元素；
- z-index 属性仅在元素定义了 position 属性且属性值不为 static 时才有效。

### CSS溢出

overflow: visible

溢出部分不被裁剪，且在区域外面显示

`overflow: hidden`（裁剪溢出部分且不可见）

`overflow:scroll` 裁剪溢出部分，但提供上下和左右滚动条供显示

关于滚动，我们还可以单独对上下或左右方向进行，添加以下代码即可

overflow-y:scroll; 



overflow:auto 溢出的部分剪裁

### 浮动

浮动可以使一个元素脱离自己原本的位置，并在父元素的内容区中向左或向右移动，直到碰到父元素内容区的边界或者其它浮动元素为止。另外，在浮动元素之后定义的文本或者行内元素都将环绕在浮动元素的一侧，从而可以实现文字环绕的效果，类似于 Word 中图文混排。

| 值      | 描述                        |
| ------- | --------------------------- |
| left    | 元素向左浮动                |
| right   | 元素向右浮动                |
| none    | 默认值，元素不浮动          |
| inherit | 从父元素继承 float 属性的值 |

另外，在使用 float 属性时还需要注意以下几点：

- 如果设置了 float 属性且属性的值不为 none 时，若 display 属性的值为 inline-table，那么 display 实际会被设置为 table，若 display 的属性值为 inline、inline-block、run-in、table-* 等值，那么 display 实际会被设置为 block，其它情况则没有变化；

- 当元素设置了绝对定位或者 display 属性的值为 none 时，float 属性无效；

- 相邻的浮动元素，如果空间足够它们会紧挨在一起，排列成一行。

  ```
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
       .a-box{
           width: 445px;
           height: 120px;
           border: 1px  solid  red;
           padding: 20px;
           overflow: hidden;
       }
       .b-box{
           width: 100px;
           height: 100px;
           border: 1px  solid  green;
           color: red;
       }
  
       .c-box{
           width: 445px;
           height: 120px;
           border: 1px  solid  red;
           padding: 20px;
           overflow: hidden;
       }
  
          .d-box{
              width: 100px;
              height: 100px;
              border: 1px solid green;
              float: left;
              color: red;
          }
  
  
      </style>
  </head>
  <body>
  
  
   <div class="a-box">
       浮动指的是让设置了 float 属性的元素脱离正常的位置，在父元素内容区中向左或向右移动，
       <div class="b-box">没有浮动的元素</div>直到碰到父元素内容区的边界或者其它浮动元素为止，
       父元素中的文本和行内元素将环绕浮动元素。
  
   </div>
  
  <div class="c-box">
      浮动指的是让设置了 float 属性的元素脱离正常的位置，在父元素内容区中向左或向右移动，
      <div class="d-box">没有浮动的元素</div>直到碰到父元素内容区的边界或者其它浮动元素为止，
      父元素中的文本和行内元素将环绕浮动元素。
  
  
  
  </div>
  
  
  </body>
  </html>
  
  ```

  元素浮动之后，会对周围的元素造成一定的影响，为了消除这种影响您可以使用 clear 属性来清除浮动，属性的可选值如下：

  

  | 值      | 描述                               |
  | ------- | ---------------------------------- |
  | left    | 左侧不允许浮动元素                 |
  | right   | 右侧不允许浮动元素                 |
  | both    | 左右两侧均不允许浮动元素           |
  | none    | 默认值，允许浮动元素出现在左右两侧 |
  | inherit | 从父元素继承 clear 属性的值        |

```
<!DOCTYPE html>
<html>
<head>
    <style>
        .box {
            width: 480px;
            height: 260px;
            border: 1px solid red;
        }
        .a-box {
            width: 50px;
            height: 150px;
            background-color: #CCC;
            float: left;
        }
        .b-box {
            width: 155px;
            height: 60px;
            border: 1px solid black;
            float: left;
        }
        .c-box {
            width: 100px;
            height: 100px;
            background-color: #007FFF;
            clear: left;
        }
    </style>
</head>
<body>
    <div class="box">
        <div class="a-box">a-box</div>
        <div class="b-box">b-box</div>
        <div class="c-box">c-box</div>
    </div>
</body>
</html>
```



### 清除浮动

```
overflow: hidden;
清除父元素的浮动效果
```

### CSS书写顺序

1. 浮动/display
2. 盒子模型  margin  border padding 
3. 文字样式

### 不透明度

专门设置图片的不透明度

​    opacity: 0.2;    

   opacity: 0.5;    

  opacity: 1; 

### CSS函数

```
url("图片网络路径") //搭配background-color一起使用
```

### CSS背景

默认的背景颜色是白色

```
background-color: ""; // 设置背景颜色
```

```

background-image: url("图片路径") //将该图片设置为背景
background-repeat: repeat-x; // 背景图片 水平平铺

background-repeat: repeat-y; // 背景图片垂直平铺

background-repeat:no-repeat; //设置不平铺

background-position: right top; //设置图片的位置 right为x轴  top为y轴


background-attachment: fixed;  //会固定在一个地方


background简写形式

background:#ffffff url('img_tree.png') no-repeat fixed right top;
```

img标签和背景图的区别

img显示重要的图片

修饰作用的使用背景图

### 通配符选择器

*{

}

### 复合选择器

1 后代选择器

语法: div p{

}

含义: 选择所有div元素下的p元素，哪怕不是直接div下的

2 子选择器

语法  div>p {

}

含义:  直接选择div下的p元素

3 并集选择器

p ,div{

}

4 交集选择器

选择p标签中class是box的选择器

p.box{

}

5 伪类选择器

让鼠标悬停的时候文字颜色是红色

```
div:hover{
    color: #2b542c;
}
任何标签都可以添加伪类
```

### CSS文本缩进

段落首行缩进

取值 单位 em   1个em等于一个字体的大小

```
   p{
       text-indent: 2em ;
   }
```



### CSS文本对齐

文本颜色

div { color:xxx;}

文本的对齐方式

text-align不止让文字居中还可以让内容居中 比如说图片

div{

text-align: center; //居中对齐

text-align: right; 右边

text-align: justif; //大文本左右两端对齐

}

{

text-decoration: none;//删除链接的下划线

}

### 居中说明

text-align 可以让文字或者图片 居中

margin 可以让标签居中

两者是不一样的

### Css装修文字线条

用于去除下划线 text-decoration:none;

h1 {text-decoration:overline;}

 h2 {text-decoration:line-through;}

 h3 {text-decoration:underline;}

文字缩进

{

text-indent: 50px;

}

| 属性                                                         | 描述                     |
| :----------------------------------------------------------- | :----------------------- |
| [color](https://www.runoob.com/cssref/pr-text-color.html)    | 设置文本颜色             |
| [direction](https://www.runoob.com/cssref/pr-text-direction.html) | 设置文本方向。           |
| [letter-spacing](https://www.runoob.com/cssref/pr-text-letter-spacing.html) | 设置字符间距             |
| [line-height](https://www.runoob.com/cssref/pr-dim-line-height.html) | 设置行高                 |
| [text-align](https://www.runoob.com/cssref/pr-text-text-align.html) | 对齐元素中的文本         |
| [text-decoration](https://www.runoob.com/cssref/pr-text-text-decoration.html) | 向文本添加修饰           |
| [text-indent](https://www.runoob.com/cssref/pr-text-text-indent.html) | 缩进元素中文本的首行     |
| [text-shadow](https://www.runoob.com/cssref/css3-pr-text-shadow.html) | 设置文本阴影             |
| [text-transform](https://www.runoob.com/cssref/pr-text-text-transform.html) | 控制元素中的字母         |
| [unicode-bidi](https://www.runoob.com/cssref/pr-text-unicode-bidi.html) | 设置或返回文本是否被重写 |
| [vertical-align](https://www.runoob.com/cssref/pr-pos-vertical-align.html) | 设置元素的垂直对齐       |
| [white-space](https://www.runoob.com/cssref/pr-text-white-space.html) | 设置元素中空白的处理方式 |
| [word-spacing](https://www.runoob.com/cssref/pr-text-word-spacing.html) | 设置字间距               |

### CSS文字行高

作用: 控制一行文字的上下行间距

如果和元素高度一致，可以使内容垂直居中

```
p{
    line-height: 50px;
    也可以写成数字
    1.5 字号的1.5倍
}
```

### CSS字体

```
{
font-size: 50px;
font-weight: 400变细 700变粗
 font-style: italic;//代表倾斜效果 如果不写默认normal
  font-family: 微软雅黑;//windows默认微软雅黑
  如果电脑有微软雅黑就用微软雅黑 如果没有就用黑体 如果没有就选任何一种无称线字体
  font-family: 微软雅黑,黑体,sans-serif;
}
简写形式
font: italic  700 66px  宋体;
font: italic  700 66px/2  宋体;
```

| [font](https://www.runoob.com/cssref/pr-font-font.html)      | 在一个声明中设置所有的字体属性       |
| ------------------------------------------------------------ | ------------------------------------ |
| [font-family](https://www.runoob.com/cssref/pr-font-font-family.html) | 指定文本的字体系列                   |
| [font-size](https://www.runoob.com/cssref/pr-font-font-size.html) | 指定文本的字体大小                   |
| [font-style](https://www.runoob.com/cssref/pr-font-font-style.html) | 指定文本的字体样式                   |
| [font-variant](https://www.runoob.com/cssref/pr-font-font-variant.html) | 以小型大写字体或者正常字体显示文本。 |
| [font-weight](https://www.runoob.com/cssref/pr-font-weight.html) | 指定字体的粗细。                     |

### 隔行换色

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
      tr {
       text-align: center;

      }

    </style>
</head>
<body>

   <!--准备表格-->
<!--      1 准备表格 表头 thead 和表体tbody
         2 绑定事件
         3 获取元素
         4 获取行的长度 表体种tr的数量 length
         5 行数遍历 for循环
         6 设置背景颜色


-->

  <table  id="tab" align="center"  border="1"  width="70%"  style="border-collapse: collapse;">
     <thead>
     <tr>

         <th>编号</th>
         <th>分类名称</th>
         <th>分类描述</th>
         <th>操作</th>

     </tr>
     </thead>
     <tbody>
       <tr>
           <td>1</td>
           <td>手机数码</td>
           <td>手机数码</td>
           <td><button>修改</button><button>删除</button></td>

       </tr>
       <tr>
           <td>2</td>
           <td>手机数码</td>
           <td>手机数码</td>
           <td><button>修改</button><button>删除</button></td>

       </tr>
       <tr>
           <td>3</td>
           <td>手机数码</td>
           <td>手机数码</td>
           <td><button>修改</button><button>删除</button></td>

       </tr>
       <tr>
           <td>4</td>
           <td>手机数码</td>
           <td>手机数码</td>
           <td><button>修改</button><button>删除</button></td>

       </tr>
     </tbody>


  </table>

  <script type="text/javascript">


      window.onload=function () {

        let tab=document.getElementById("tab")
          tab.children[0].style.backgroundColor="navajowhite"

          let  tbody=tab.tBodies[0].children.length;
          for(let i=0;i<tbody;i++){
              if(i%2==0){
                  tab.tBodies[0].children[i].style.backgroundColor="skyblue"
              }else {
                  tab.tBodies[0].children[i].style.backgroundColor="pink"
              }
          }
      }

  </script>



</body>
</html>

```

### 全选全不选

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
      tr {
       text-align: center;

      }

    </style>
</head>
<body>

   <!--准备表格-->
<!--      1 准备表格 表头 thead 和表体tbody
         2 绑定事件
         3 获取元素
         4 获取行的长度 表体种tr的数量 length
         5 行数遍历 for循环
         6 设置背景颜色


-->

  <table  id="tab" align="center"  border="1"  width="70%"  style="border-collapse: collapse;">
     <thead>
     <tr>
        <th><input  type="checkbox" id="all"/>&nbsp;<button onclick="checknos()">反选</button> </th>
         <th>编号</th>
         <th>分类名称</th>
         <th>分类描述</th>
         <th>操作</th>

     </tr>
     </thead>
     <tbody>
       <tr>
           <td><input type="checkbox" name="ck" /></td>
           <td>1</td>
           <td>手机数码</td>
           <td>手机数码</td>
           <td><button>修改</button><button>删除</button></td>

       </tr>
       <tr>
           <td><input type="checkbox" name="ck" /></td>
           <td>2</td>
           <td>手机数码</td>
           <td>手机数码</td>
           <td><button>修改</button><button>删除</button></td>

       </tr>
       <tr>
           <td><input type="checkbox" name="ck" /></td>
           <td>3</td>
           <td>手机数码</td>
           <td>手机数码</td>
           <td><button>修改</button><button>删除</button></td>

       </tr>
       <tr>
           <td><input type="checkbox" name="ck" /></td>
           <td>4</td>
           <td>手机数码</td>
           <td>手机数码</td>
           <td><button>修改</button><button>删除</button></td>

       </tr>
     </tbody>


  </table>

  <script type="text/javascript">

     document.getElementById("all").onclick = function () {
            let flag=document.getElementById("all").checked;

            let cks=document.getElementsByName("ck");
                   for(let i=0;i<cks.length;i++){
                       if(flag ){
                           cks[i].checked=true

                       }else {
                           cks[i].checked= false
                       }
                   }


     }


     function checknos() {
        let cks=document.getElementsByName("ck");

        for(let i=0; i<cks.length;i++){
            if(cks[i].checked){
                cks[i].checked =false
            } else {
                cks[i].checked =true
            }
        }
     }

  </script>



</body>
</html>

```

### CSS 标签水平居中

```
   margin: 0 auto;
```



# css margin缩写简写巧妙记法

```
CSS样式上下、左右、上下左右缩写简写优化

一、记忆要领：
上(top)
左(left) 右(right)
下(bottom)
按照上面图形：顺时针排序
即为 margin: top right bottom left

二、举例说明

top right bottom left（值都不同）
margin-top: 10px;
margin-right: 20px;
margin-bottom:30px;
margin-left: 40px;
缩写：margin:10px 20px 30px 40px;

top right bottom left（值相同）
margin-top: 10px;
margin-right: 10px;
margin-bottom:10px;
margin-left: 10px;
缩写：margin:10px;

top right bottom left （right left 值相同 ）
margin-top: 10px;
margin-right: 20px;
margin-bottom:30px;
margin-left: 20px;
缩写：margin:10px 20px 30px;
附加：
原始：margin-top:5px; margin-bottom:6px; margin-left:4px
缩写：margin:5px 0 6px 4px或margin:5px auto 6px 4px

top right bottom left （top bottom 值相同 无）
margin-top: 10px;
margin-right: 20px;
margin-bottom:10px;
margin-left:30px;
缩写：margin:10px 20px 30px;
说明：应该没有此类缩写，上下一样有点不可实现上下一样的话就是垂直居中了对吧？不然和第3点缩写后都是3个值，看不出来是左右相同还是上下相同的缩写
有不对的地方欢迎大家指正！

top right bottom left （top 和bottom 值相同，right 和left 值相同 ）
margin-top: 10px;
margin-right: 20px;
margin-bottom:10px;
margin-left:20px;
缩写：margin:10px 20px;
附加：
margin:0 auto;横向居中； （上下为0，左右自动，则水平居中）
margin：auto 0；纵向居中；（左右为0，上下为0，则垂直居中

padding的距离设置缩写同理哦

作者：一笑倾城Tan
链接：https://www.jianshu.com/p/cd14bbb3748a
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
```

