# cocos-ts-文档说明

## 2D游戏制作

## 监听鼠标事件

```
 input.on(Input.EventType.MOUSE_UP, this.onMouseUp, this);


 onMouseUp(event: EventMouse) {
    if (event.getButton() === 0) {
        this.jumpByStep(1);
    } else if (event.getButton() === 2) {
        this.jumpByStep(2);
    }
}
```

## update方法

    update (deltaTime: number) {}

`update` 方法会被引擎以一定的时间间隔调用，比如帧率为 30 每秒时，则每秒会调用 `update` 30 次，这个方法的作用是为了能够通过特定的时间间隔来尽量模拟现实中时间连续的现象。

## 获取node的位置信息

private _curPos:Vec3=new Vec3();

this.node.getPosition(this._curPos);

获取信息赋值给 this._curPos

## 赋值node位置信息

this.node.setPosition(this._deltaPos)

## 清楚节点全部信息

     this.node.removeAllChildren();

## 计算目标位置

     Vec3.add(this._curPos, this._curPos, this._deltaPos);
     赋值给第一个变量

## 创建动画流程

### 1 第一步给精灵动画组件

add Compoent  - Animation

### 2 绘制动画并将动画添加到精灵上

### 3

代码层面
    @property(Animation)
        BodyAnim:Animation = null;
     this.BodyAnim.play('oneStep');

组件层面要将精灵挪到BodyAnim

获取动画时长的api

const  state=this.BodyAnim.getState(clipName);  
this._jumpTime=state.duration;



if(step == 1){  
    this.BodyAnim.play('oneStep')  
}else if(step == 2) {  
    this.BodyAnim.play('twoStep')  
}

## 给按钮添加绑定事件

let  btn=this.node.getChildByName("Button");

btn.on(Button.EventType.CLICK,this.onBtnStartupClicked,this);

onBtnStartupClicked(){  
    console.log("onBtnStartupClicked")  
}

## 加载场景

director.loadScene("game");

## 暂停游戏

director.pause();



恢复游戏

director.resume();

是否暂停

director.isPaused()

## 节点设置index

panel.setSiblingIndex(index)

## 节点移除

panel.removeFromParent();

## 监听鼠标开始事件

input.on(Input.EventType.TOUCH_START,this.onTouchStart,this)

获取鼠标按下的位置

// 鼠标 手指按下  
onTouchStart(eventTouch: EventTouch){  
    let x=eventTouch.touch.getUILocationX()  
    let y=eventTouch.touch.getUILocationY()  
    this.stickBg.setWorldPosition(x,y,0);  
    // 将 stickBg 移动到鼠标按下的位置

}



## 世界坐标和本地坐标有什么不同

世界坐标可以理解为全局坐标，本地坐标是相对于父节点坐标的



## 监听手指事件

在电脑上运行手指事件等于鼠标事件

### 手指按下监听

input.on(Input.EventType.TOUCH_START,this.onTouchStart,this)

```
 onTouchStart(eventTouch: EventTouch) {
      let x=eventTouch.touch.getUILocationX()
        let y=eventTouch.touch.getUILocationY()
    获取x y 

 }
```

### 手指滑动距离

input.on(Input.EventType.TOUCH_MOVE,this.onTouchMove,this)

```
 onTouchMove(eventTouch: EventTouch) {

         let x=eventTouch.touch.getUILocationX()
        let y=eventTouch.touch.getUILocationY()

 }
```

### 手指停止移动

```
input.on(Input.EventType.TOUCH_END,this.onTouchEnd,this)

onTouchEnd(){

}
```

## 向量-滑动距离

我个人认为 对于本地坐标系而言  本地坐标系就是向量  向量的长度就是手指滑动的距离

### 如何动态的变换向量的长度

#### 1 向量需要归一化

```
localPository.normalize();
```

向量归一化后方便不变 长度为1

#### 2 向量乘以标量来进行 来得出新的位置

官方提供的api

localPository.multiplyScalar(数字)

## 获取节点上的ui组件

```
this.sliderBgmVolume=this.node.getChildByName("Bgm").getComponent(Slider)
```

这里获取的是 Slider组件

## 获取节点的子节点 简略写法

```
his.progressbarBgmVolum=this.node.getChildByPath("Bgm/ProgressBar").getComponent(ProgressBar);
```

## 进度条事件绑定

```
 this.sliderBgmVolume.node.on('slide',this.onBgmVolumeChange,this);
  this.progressbarBgmVolum=this.node.getChildByPath("Bgm/ProgressBar").getComponent(ProgressBar);
   onBgmVolumeChange(value:Slider) {
     this.progressbarBgmVolum.progress=value.progress
 }
```

## 缓存使用

sys.localStorage.setItem(key,value.toString())

let str=sys.localStorage.getItem(key);

## 如何动态切换图片

## 1 先加载目录资源，它通常用于加载项目中的资源文件，例如图像、声音、预制件等

```
resources.loadDir("路径",()=>{


})
```

因为loadDir是一个异步方法，所以提供回调函数

## 2 根据动态参数，拼接url，然后通过resources.get()获取图片贞

```
  const spriteFrame=resources.get(path,SpriteFrame)
```

SpriteFrame是类型 图片贞的类型

## 3 获取二D组件，并且设置他的spriteFrame

```
 let sprite=spriteNode.getComponent(Sprite)

                sprite.spriteFrame=spriteFrame
```

## 4 设置可见和索引

```
 spriteNode.active=true
 spriteNode.setSiblingIndex(i)
```

## 内存池的使用

### 1 创建内存池

```
 numberPool: Pool<Node> =null;

      this.numberPool=new Pool(():Node=>{
             let node=   instantiate(this.numberPrefab)
            this.node.addChild(node)
            node.active=false
            return node

        },5,(node:Node)=>{
                node.removeFromParent()
        })
```

## 2 内存池取出对象

```
let spriteNode= this.numberPool.alloc()
```

### 3 内存池归还对象

```
this.numberPool.free(child)
```

### 4 内存池销毁对象

```
this.numberPool.destroy()
```

## GamePlay设计原则

# Cocos基本操作

alt+鼠标左键 可以360 度查看节点

坐标轴用法 点击哪个轴，就从那个轴往下看

crtl shift  f 可以将屏幕切换为摄像机，这是一个小技巧，先将编辑器调整好，再选择摄像机

点击crtl shift f 就可以切换视角

## 组件化开发流程

### 1 新建一个组件类，被引擎识别

创建一个ts组件类

一个组件类模板里面有什么？

Component  组件类的基类

Node 场景里面的节点  组件实例的载体

_decorator  装饰器名称

### 2 new  类的实例 挂载到组件实例节点上

将脚本拖拽到组件上，就完成了组件类的实例化

### 3 分析一下我们的引擎如何调用我们写代码，代码入口---》插入你的代码

游戏引擎先把组建的逻辑处理完，再交给GPU绘制场景

1 遍历每个节点 ，找到节点上每个实例，然后调用组件实例的方法

我们的脚本实例都是挂载节点上，由节点实例

## 节点-》组件实例-》组件实例的方法

### 1 第一次加载这个组件实例到节点的时候

会调用组件实例 onLoad()  方法

因为是第一次加载调用，这个是初始化数据的一个地方

## 2 第一次画面刷新之前，组件实例 Start()方法

## 3 每次画面刷新的时候： 组件实例update方法

基于时间来迭代变化

假如我们的速度是100/秒  100*deltaTime

  update(deltaTime: number) {

    }

### 4 绑定到编辑器上了的都是组件实例的成员

## 组件与节点的关系

组件里面的this.node就是指向挂载这个组件的节点

### 获取节点的父亲节点

let parent=  this.node.parent

### 获取节点的孩子节点

this.node.children   获取孩子节点数组

可以遍历

 this.node.getChildByName('子节点名称')

### 全局查找

find('/Cube')

### 通过节点查找组件实例

 let cc= this.node.getComponents('/Cube')

### 添加组件实例

    this.node.addComponent(GameMgr)

# 3维向量

## 创建3维向量

两种构造函数

 let v=new Vec3(3,4,5);



let v2=v3(7,7,8)



# 节点平移 世界 局部

## 平移

### 获取世界坐标和局部坐标 并且修改值

let ball=this.node.getChildByName('Sphere');

        console.log(ball.worldPosition,ball.position)

Readonly 代表只读 尽量不要去修改

### 世界缩放和局部缩放

console.log(ball.worldScale,ball.scale)

### 欧拉角旋转

欧拉旋转是直观的三维旋转--分别绕每个轴来旋转来得到我们的物体空间摆放位置

对应访问的 x y z 有一个固定顺序

内部---》欧拉角---》四元素



两种旋转

a 欧拉角Vec3-四元素

b 四元数旋转 Quat



世界旋转：基于世界坐标系

局部旋转： 基于父类旋转

设置本地欧拉    let v=new Vec3(0,0,0)    

        ball.eulerAngles=v



设置本地四元数

 let v:Quat=new Quat  

        ball.rotation=v

相对于世界的旋转---》世界的欧拉角

 this.node.setWorldRotationFromEuler(0,60,0) //这个是我们写代码用的



案例

    update(deltaTime: number) {

        let pos = this.node.getPosition();



        pos.z+=2 * deltaTime;



        this.node.setPosition(pos)



        let rot:Vec3=this.node.eulerAngles;

        rot.y+=(30 * deltaTime)

        this.node.setRotationFromEuler(rot)



    }

## 监听系统事件

调用引擎接口，给引擎传递一个函数对象

### 触摸事件(鼠标事件)

监听触摸事件，在回调函数里面写逻辑

系统事件会有一个全局唯一的单例  systemEvent  类型 SystemEvent

 systemEvent.on(事件类型，事件回调，target)

新版本

    onTouchStart(touch: Touch,event: EventTouch){

        let l=touch.getLocation()

        console.log('触摸按下')

    }

触摸坐标以左下角为原点

 let v2=touch.getDelta()

这次触摸 减去上一次触摸得到的二维向量

#### 触摸按下

input.on(Input.EventType.TOUCH_START,this.onTouchStart,this)

#### 触摸弹起

 systemEvent.on(SystemEvent.EventType.TOUCH_END,this.onTouchEnd,this)

#### 触摸移动

systemEvent.on(SystemEvent.EventType.TOUCH_MOVE,this.onTouchMove,this)

### 键盘事件

    systemEvent.on(SystemEvent.EventType.KEY_DOWN,this.onKeyDown,this);

  onKeyDown(event: EventKeyboard)  {

 console.log(event.keyCode) // 这就是按键码

    }

#### 键盘按下

 systemEvent.on(SystemEvent.EventType.KEY_DOWN,this.onKeyDown,this);

#### 键盘弹起

systemEvent.on(SystemEvent.EventType.KEY_UP,this.onKeyUp,this);

### 重力感应事件

## 删除监听

systemEvent.off(SystemEvent.EventType.TOUCH_START,this.onTouchStart,this)

这是删除三者相同的

要记得一定要释放

## 预制体

实例化

 instantiate()

## MeshRenderer

网格绘制组件，功能绘制一个3D物体

### Materials

材质属性

#### 创建材质对象

新建一个material

### 材质切换

1 使用编辑器切换

 @property(Material)

    private mt:Material=null;

   let mr:MeshRenderer=  this.node.getComponent(MeshRenderer)

      mr.setMaterial(this.mt,0);





2 使用代码来切换

### LMS

光照烘培相关属性

### 阴影

阴影相关属性

Shadow Casting Mode

Receive Shadow

#### 开启阴影

场景设置里面  **shadows**选项 组件中勾选 **Enabled** 属性

第二步 接收物体阴影的选项要开启 

若阴影类型是 **ShadowMap**，还需要将 MeshRenderer 组件上的 **ReceiveShadow** 属性设置为 **ON**。

第三步 阴影本生物体  shadow casling mode 开启

## 三D动画模型

 更节电---》Skeletal Animation  ---》动画组件，提供接口给用户来播放我们的动画

Body这个节点就是我们模型的节点---》带了一个组件---》蒙皮网格渲染器；也叫SkinnedMeshRenderer

骨骼节点相关位置信息: 节点绑定我们的动画--》把节点绑定到我们的骨骼--》骨骼节点

### SkinnedMeshRenderer

Materials: 材质

Skeleton： 骨骼信息

Skinning Root: 动画组件所在节点

### Skeletal Animation

clips: 动画信息

play OnLoad: 是否在加载的时候播放

sockets  挂自己的节点到动画骨骼上，让节点随着动画运动

use baked animation 是否使用预烘培动画

### 如何播放3d骨骼动画

this.anim=this.node.getComponent(SkeletalAnimation)

            this.anim.play("die")

 //平滑过渡

            this.anim.crossFade("die",0.3)

### 绑定节点到动画骨骼上

现在动画骨骼上创建节点

然后挂到sockets上面

## 天空盒和雾气

创建天空盒 目录下创建cubemap 六个面的纹理



场景填充天空盒

选中摄像机，选项Clear Flags 选择 SKYBOX

选择场景的Skybox 选项 选择Envmap  将cubemap关联过去

### 雾

特点： 远处看比较浓，近处看 比较清晰

场景选择 fog  设置type为LINEAR

## 摄像机使用及分组管理

摄像机有两种模式

参数Priority  ：值越小 就优先绘制这个摄像机的内容 在多台摄像机的情况下

### 透视成像

相机设置 Projection== RER。。。

Visibility： 这个摄像机拍摄哪些物体--列举出来当前的物体类型，打了勾的就表示我们的摄像机拍摄哪些物体，物体的类型在哪里看 每个节点都有一个Layer，那个就是类型，我们也可以自己添加类型

Clear Flags：天空盒样式

Fov： 视角

| 属性名称          | 说明                                                                                                                       |
| ------------- | ------------------------------------------------------------------------------------------------------------------------ |
| Priority      | 相机的渲染优先级，值越小越优先渲染                                                                                                        |
| Visibility    | 可见性掩码，声明在当前相机中可见的节点层级集合                                                                                                  |
| ClearFlags    | 相机的缓冲清除标志位，指定帧缓冲的哪部分要每帧清除。包含：<br>DONT_CLEAR：不清空；<br>DEPTH_ONLY：只清空深度；<br>SOLID_COLOR：清空颜色、深度与模板缓冲；<br>SKYBOX：启用天空盒，只清空深度 |
| ClearColor    | 指定清空颜色                                                                                                                   |
| ClearDepth    | 指定深度缓冲清空值                                                                                                                |
| ClearStencil  | 指定模板缓冲清空值                                                                                                                |
| Projection    | 相机投影模式。分为 **透视投影（PERSPECTIVE）** 和 **正交投影（ORTHO）**                                                                        |
| FovAxis       | 指定视角的固定轴向，在此轴上不会跟随屏幕长宽比例变化                                                                                               |
| Fov           | 相机的视角大小                                                                                                                  |
| OrthoHeight   | 正交模式下的视角                                                                                                                 |
| Near          | 相机的近裁剪距离，应在可接受范围内尽量取最大                                                                                                   |
| Far           | 相机的远裁剪距离，应在可接受范围内尽量取最小                                                                                                   |
| Aperture      | 相机光圈，影响相机的曝光参数                                                                                                           |
| Shutter       | 相机快门，影响相机的曝光参数                                                                                                           |
| Iso           | 相机感光度，影响相机的曝光参数<br>**Aperture**，**Shutter** 和 **Iso** 属性请参考下方 **曝光量** 获取更多信息                                             |
| Rect          | 相机最终渲染到屏幕上的视口位置和大小                                                                                                       |
| TargetTexture | 指定相机的渲染输出目标贴图，默认为空，直接渲染到屏幕                                                                                               |

一般3D场景都使用透视场景

### 正交成像

相机设置 Projection==ORTHO

一般2Dui使用

正交相机不会随着距离的远近变化

## 光源使用

### 太阳光

遥远的距离发射；没有焦点

### 聚光灯

## unlit Shader

a 创建一个材质，

b 将材质的属性Effect 设置 builtin-unilt  这个不会受光照影响

c 选择use  texttrue  设置纹理

d 节点上选择这个材质



参数

Effect:  关联的shader主要参数详解

Technique: 渲染队列

USE INSTANCING: 是否开启核批

Pass: GPU渲染管线

## PBR物理渲染

内置的PBR Shader=== builtin-standard

USE NORMAL MAP: 法线贴图

USE ALBEDO MAP：漫反射贴图

USE PBR MAP

## 物理引擎

### 碰撞器

boxColi

碰撞器 cocos内置一些物体碰撞器

立方体碰撞器

矩形碰撞器

胶囊体碰撞器

地形碰撞器

Mesh碰撞器



每个物理形状都会有一个物理材质

Material属性：如果不设定就用项目默认 如果想另外设置 新建Phy Mate 然后设置挂载上去

Is Trigger ：触发器 勾选上 那么这个物理形状，不会对其他的物体的运动变化造成影响，只会发生碰撞检测，

球体性能事最好的

### 刚体

RigBody

动态物体除了有形状还要加一个刚体组件

给物体设置刚体以后，他会受物理特性影响  比如项目设置里面设置 -y 那么他会自由下落

group: 刚体分组

Type：static 静态刚体



虽然静态物体不需要加RigBody 但是要配置他的碰撞类型，设置他的分组

#### 刚体常用操作

001 搭建场景

a: 有物理重力

b: 加一个我们的地面；---> 平面 + 物理碰撞器

c: 添加一个boxCollider对的物体+ 刚体组件；

002  给刚体添加一个物理材质，以及物理材质相关参数

a 新建一个物理材质

b 配置物理材质的参数

003 给刚体一个线性速度

 this.body=this.node.getComponent(RigidBody)

        this.body.useGravity=true

            this.body.setLinearVelocity(v3(1,0,0))

线性速度 米/秒

004 给刚体一个角速度

this.body=this.node.getComponent(RigidBody)

        this.body.useGravity=true

        

            this.body.setAngularVelocity(v3(0,120,0))

角速度的单位是弧度



005 给刚体一个力

this.body=this.node.getComponent(RigidBody)

        this.body.useGravity=true

 this.body.applyForce(v3(-100,0,0))


